This article is dedicated to the description of Java JVM architecture and its memory model.

# Java environments
Java proposes 2 environments necessary to start any Java work:
* JRE (Java Runtime Environment): the minimum environment needed for running a Java application with no support for programming. It includes JVM (Java Virtual Machine) and deployment tools.
* JDK (Java Development Kit): the complete development environment used for developing and executing Java applications. It includes both JRE and development tools.

JRE is dedicated to users, while JDK is dedicated to programmers.

# How Java works
Java has been designed with the concept of "Write once, Run anywhere".

Java source codes are compiled into an intermediate state called bytecode (a `.class` file) using the Java Compiler (**javac**) which comes inbuilt with JDK. 

This bytecode is in hexadecimal format and JVM can interpret these instructions (without further recompilations) into native machine language which can be understood by the OS and underlying hardware platform.

Therefore, bytecode acts as a platform-independent intermediary state which is portable among any JVM regardless of underlying OS and hardware architecture.

However, since JVMs are developed to run and communicate with the underlying hardware & OS structure, we need to select the appropriate JVM version for our OS version (Windows, Linux, Mac) and processor architecture (x86, x64).

# Java memory allocation

The Java Memory Allocation is divided into following sections :

1. **Heap**: The Heap section contains Objects (may also contain reference variables).
2. **Stack**: The Stack section of memory contains methods, local variables, and reference variables.
3. **Code**: The code section contains your bytecode.
4. **Static**: The Static section contains Static data/methods.

This division of memory is required for its effective management.

* Java Heap Space

Java Heap space is used by java runtime to allocate memory to Objects and JRE classes. Whenever we create an object, it’s always created in the Heap space.

**Garbage Collector** runs on the heap memory to free the memory used by objects that don’t have any reference. Any object created in the heap space has global access and can be referenced from anywhere of the application.

* Java Stack memory

Java Stack memory is used for the execution of a thread. They contain method-specific values that are short-lived and references to other objects in the heap that is getting referred from the method.

Stack memory is always referenced in LIFO (Last-In-First-Out) order. Whenever a method is invoked, a new block is created in the stack memory for the method to hold local primitive values and reference to other objects in the method.

As soon as the method ends, the block becomes unused and becomes available for the next method.

> Stack memory size is very less compared to Heap memory.

-|Stack | Heap
----------|----------|----------
Application | Each thread operates its own stack: thread safe. One at a time during execution | Not thread safe. Shared resources
Size | Limited size depending upon OS, smaller then Heap | No size limit from OS. User-defined or by default
Storage | Only primitive variables and objects references created in the Heap | New created objects
Order | LIFO (Last-In-First-Out) memory allocation technique | Complex memory management technique (Young / Old / Tunered / Permanent generations)
Life | Exists as long as the current method is running | Exists as long as the current application is running
Efficiency | Much faster to allocate compared to Heap | Slower to allocate compared to Stack
Allocation/Deallocation | automatic when the method is called and returns result | when new objects created and when GC runs

Heap and Stack illustration from Baeldung.com:

![Heap and Stack example](./images/heap_stack.jpeg)



# Main concepts of JVM threads

JVM has 2 categories of threads:

* Application threads: threads created first by the call of `main()` method in each application running under JVM, but not only. This main threads create other application threads. All bring the programmatic logic.

* System threads (shared by application threads): threads to execute common & background tasks.

The major system threads are as follows.

* Compiler threads: At runtime, compilation of bytecode to native code is undertaken by these threads. 

* GC threads: All the GC related activities are carried out by these threads.

* Periodic task thread: The timer events (i.e. interrupts) to schedule execution of periodic operations are performed by this thread.

* Signal dispatcher thread: This thread receives signals sent to the JVM process and handle them inside the JVM by calling the appropriate JVM methods.

* VM thread: As a pre-condition, some operations need the JVM to arrive at a safe point where modifications to the Heap area does no longer happen. Examples for such scenarios are “stop-the-world” garbage collections, thread stack dumps, thread suspension and biased locking revocation. These operations can be performed on a special thread called VM thread.

# JVM Architecture

The JVM architecture is first a specification. Its implementation defers depending on the vendor.

The important thing to get in mind is that **JVM resides on the RAM**.

Here is its architecture:

![Architecture of JVM](./images/JVM_archi.png)

## Class Loader Subsystem

It loads, links, and initializes the class files for the first time on to the RAM. This step is known as dynamic class loading. 

### Loading

It loads class on to memory starting with the main classes and following with subsequent classes:
* static reference to a class in the bytecode (ex: `System.out`)
* new object (ex: `new MyClass()`)

There is 3 class loaders with inheritance property:

* **Bootstrap Class Loader**: loads standard JDK classes from rt.jar (core Java API) classes present in the bootstrap path — $JAVA_HOME/jre/lib directory (e.g. `java.lang.*` package classes). It is implemented in native languages like C/C++ and acts as parent of all class loaders in Java.

* **Extension Class Loader**: delegates class loading request to its parent, Bootstrap and if unsuccessful, loads classes from the extensions directories $JAVA_HOME/jre/lib/ext or any other directory specified by the java.ext.dirs system property. This Class Loader is implemented in Java by the `sun.misc.Launcher$ExtClassLoader` class.

* **System/Application Class Loader**: loads application specific classes from system class path, that can be set while invoking a program using -classpath (`-cp`) command line options. It internally uses Environment Variable which mapped to java.class.path. This Class Loader is implemented in Java by the `sun.misc.Launcher$AppClassLoader` class

A programmer can create his own Class Loader. This guarantees the independence of applications through class loader delegation model. This approach is used in web application servers like Tomcat to make web apps and enterprise solutions run independently.

Each class Loader has its own namespace. It loads classes using Fully Qualified Class Names (FQCN) but the same FQCN in several namespaces is loaded in each namespace.

### Linking

This step starts when classes and interfaces are completely loaded.

Linking occurs in 3 stages:

* **Verification**: ensure the correctness of .class file (code according to Java Language Specification and valid bytecode according to JVM specifications). This is the most complicated and time consuming process of the class loading processes. Even though linking slows down the class loading process, it avoids the need to perform these checks for multiple times when executing bytecode, hence makes the overall execution efficient and effective. If verification fails, it throws runtime errors (`java.lang.VerifyError`). 

Checks at this stage:

```
- consistent and correctly formatted symbol table
- final methods / classes not overridden
- methods respect access control keywords
- methods have correct number and type of parameters
- bytecode doesn’t manipulate stack incorrectly
- variables are initialized before being read
- variables are a value of the correct type
```
* **Preparation**: allocate memory for static storage and any data structures used by the JVM such as method tables. Static fields are created and initialized to their default values, however, no initializers or code is executed at this stage as that happens as part of initialization.

* **Resolution**: replace symbolic references from the type with direct references. It is done by searching into method area to locate the referenced entity

### Initializing (Initialization)

This step starts the initialization logic of each loaded class or interface (ex: calling the constructor of a class). 

Classes must be careful to implement properly the synchronized since JVM is multi-threaded to avoid multiple threads to initialize the same class or interface at the same time (i.e. make it thread safe).

This is the final phase of class loading where all the static variables are assigned with their original values defined in the code and the static block will be executed (if any). 

This is executed line by line from top to bottom in a class and from parent to child in class hierarchy.

## Runtime Data Area

When a JVM program runs on an OS, Runtime Data Area acts as memory area where all the binary data loaded by Class Loader Subsystem is stored.

For every loaded `.class` file , JVM creates exactly one object of Class to represent the file in the Heap memory as defined in `java.lang` package. This Class object can be used to read class level information.

### Method Area (Shared among threads)

All JVM threads share this same Method area, so the access to the Method data and the process of dynamic linking must be thread safe.

Method Area stores class level data (including static variables): classloader reference, runtime constant pool, field data, method data, method code.

### Heap Area (Shared among Threads)

This is also a shared resource (only 1 heap area per JVM). Information of all objects and their corresponding instance variables and arrays are stored in the Heap area. Since the Method and Heap areas share memory for multiple threads, the data stored in Method & Heap areas are not thread safe. Heap area is a great target for GC.

### Stack Area (per Thread)

Each time a thread start, a corresponding Runtime Stack is created. Inside the Runtime Stack, each method call push that entry on the top of the Runtime Stack (called Stack Frame).

![Stack Frame](./images/Stack_Frame.png)

The frame is removed (destroyed) when the method returns normally or if an uncaught exception is thrown during the method invocation. Also note that if any exception occurs, each line of the stack trace (shown as a method such as `printStackTrace()`) expresses one stack frame. The Stack area is thread safe since it is not a shared resource.

Each Stack Frame brings:
* **Local variable array**: array where the 0 index is the reference of the class instance where the method belongs. 1 index represents the method parameters, the other indexes represent the local variables of the method.
* **Operand Stack**: runtime space dedicated for each method to exchange data between the Operand stack and the local variable array (pushes or pops other method invoke results). The size of Operand Stack is determined during compiling process.During method runtime.
* **Frame data**: All symbols related to the method are stored here. For exceptions, the catch block information will also be maintained in the frame data.

### PC Registers (per Thread)

A Program Counter (PC) Register is created when a thread starts to hold the address of currently-executing instruction (memory address in the Method area). Once the execution finishes, the PC register gets updated with the address of next instruction.

For native methods, the PC is undefined.

### Native Method Stack (per Thread)

A Java thread has always its native operating system thread counterpart. Means that when a Java Thread starts, OS creates a native thread also to store native method information (often written in C/C++) invoked through JNI (Java Native Interface).

Once the native thread has been created and initialized, it invokes the `run()` method in the Java thread. When the `run()` method returns, uncaught exceptions (if any) are handled, then the native thread confirms whether the JVM needs to be terminated as a result of the thread terminating. When the thread terminates, all resources for both the native and Java threads are released.

The operating system is therefore responsible for scheduling all threads (Java & natives) and dispatching them to any available CPU.

## Execution Engine

Execution Engine executes the instructions in the bytecode line-by-line by reading the data assigned to above runtime data areas.

### Interpreter

It interprets and executes the bytecode. Executing the interpreted results is slower than interpreting bytecode and the disadvantage is that when one method is called multiple times, each time a new interpretation and a slower execution are required.

### Just-In-Time (JIT) Compiler

Oracle JIT Compiler implementation (HotSpot) has been released in 2006 (Java 7). Oracle has 2 implementations of their standard Java VMs with a popular JIT compiler model called Hotspot Compiler:

* Oracle Java Hotspot Client VM is the default VM technology for Oracle JDK and JRE. It is tuned for best performance when running applications in a client environment by reducing application start up time and memory footprint.
* Oracle Java Hotspot Server VM is designed for maximum program execution speed for applications running in a server environment. The JIT compiler used here is called Advanced Dynamic Optimizing Compiler and uses more complex and diverse performance optimization techniques. The Java HotSpot Server VM is invoked by using the server command line option (`java -server`). This is the default HotSpot for 64-bit architectures.

JIT Compiler compensate the default of Interpreter to avoid redundant operations in case of multiple method calls.

JIT Compiler compiles the entire bytecode to native code (machine code). Then for repeated method calls, it directly provides the native code and the execution using native code is much faster than interpreting instructions one by one. The native code is stored in the cache, thus the compiled code can be executed quicker.

For a code segment that executes just once, it is better to interpret it instead of compiling. Thus, JIT compiler internally checks the frequency of each method call and decides to compile each only when the selected method has occurred more than a certain level of times. This idea of adaptive compiling has been used in Oracle Hotspot VMs. See [docs.oracle.com](https://docs.oracle.com/javase/8/docs/technotes/guides/vm/server-class.html).

### Garbage Collector (GC)

The JVM considers an object alive as long as it is referenced. Since Java 8, Garbage Collector First (G1) is the default GC. The G1 collector is a server-style garbage collector, targeted for multi-processor machines with large memories. The G1 collector achieves high performance and pause time goals through several techniques.

The first focus of G1 is to provide a solution for users running applications that require large heaps with limited GC latency. This means heap sizes of around 6GB or larger, and stable and predictable pause time below 0.5 seconds.

Once it is no longer referenced and therefore not reachable by the application code, the garbage collector destroy it and reclaims the unused memory.

Applications running today with either the `CMS` or the `ParallelOld` garbage collector would benefit switching to G1 if the application has one or more of the following traits:

* More than 50% of the Java heap is occupied with live data.
* The rate of object allocation or promotion varies significantly.
* Undesired long garbage collection or compaction pauses (longer than 0.5 to 1 second)

G1 is planned as the long term replacement for the Concurrent Mark-Sweep Collector (CMS). Comparing G1 with CMS, there are differences that make G1 a better solution. One difference is that G1 is a **compacting collector**. G1 compacts sufficiently to completely avoid the use of fine-grained free lists for allocation, and instead relies on regions. 

In general, garbage collection happens in background task but:

* GC process is not efficient because by nature most of the newly created objects will become unused
* Long-lived objects are most likely to be in use for future GC cycles too

To solve the above issues, new objects are stored in separate generational spaces of the Heap in a way that each generational space indicates the lifetime of objects stored in it. Then, the garbage collection happens in 2 major phases called **Minor GC** and **Major GC** and objects are scanned and moved among generational spaces (Eden -> S0 -> S1 -> Old Generation) before complete deletion. 

> All the Garbage Collections are **“Stop the World”** events because all application threads are stopped until the operation completes.

Since Young generation keeps short-lived objects, Minor GC is very fast and the application doesn’t get affected by this.

However, Major GC takes a long time because it checks all the live objects. Major GC should be minimized because it will **make your application unresponsive for the garbage collection duration**. So if you have a responsive application and there are a lot of Major Garbage Collection happening, you will notice timeout errors.

The duration taken by garbage collector depends on the strategy used for garbage collection. That is why it is necessary to monitor and tune the garbage collector to avoid timeouts in the highly responsive applications.

GC uses in reality Mark & Sweep model:

* **Mark**: identify and mark all object references (starting with the GC roots) that are still used and reachable (live objects), and the rest is considered garbage.
* **Sweep**: traverse Heap and find unoccupied spaces between the live objects, these spaces are recorded in a free list and are made available for future object allocation.

Reachable objects are objects accessible from outside of the Heap by what we call Garbage Collection roots. There is several types of GC roots such as Local variables, Active Java threads, Static variables, JNI References etc. 

As long as our object is directly or indirectly referenced by one of these GC roots and the GC root remains alive, our object can be considered as a reachable object. The moment our object loses its reference to a GC root, it becomes unreachable, hence eligible for garbage collection deletion (at this step GC destroys object and calls its `finalize()` method).

Garbage collector only destroys the objects that are unreachable. It is an automatic process happening in the background and in general programmers are not supposed to do anything regarding it.

However, object can immediately become unreachable if one of the following cases occurs without **waiting for GC Sweeping**:

* Nullifying the reference variable. Ex: `myVariable = null`
```java
// p1 is no longer in use
// make p1 eligible for gc
p1 = null;
 
// calling garbage collector
System.gc(); // p1 will be garbage-collected
```
* Re-assigning the reference variable: When a reference id of one object is referenced to a reference id of some other object, then the previous object will have no reference to it any longer
```java
// create two Person objects
// new operator dynamically allocates memory for an object and returns a reference to it
Person p1 = new Person("John Doe");
Person p2 = new Person("Jane Doe");
 
// some meaningful work happens while p1 are p2 are in use
...
// p1 is no longer in use
// make p1 eligible for gc
p1 = p2; 
// p1 now referred to p2
 
// calling garbage collector
System.gc(); // p1 will be garbage-collected
```
* Object created inside a method. When such a method is popped out from the Stack, all its members die and if some objects were created inside it, then these objects also become unreachable
* Anonymous object: object reference id is not assigned to a variable
* Objects with only internal references (Island of Isolation)

> **Call GC programmatically**
>
> Using `System.gc()` method or Using `Runtime.getRuntime().gc()` method


> **Memory leaks**
> 
> The garbage collector of the JVM releases Java objects from memory as long as no other object refers to this object. If other objects still hold references to these objects, then the garbage collector of the JVM cannot release them.

## Java Native Interface (JNI)

JNI enables the JVM to interact with C/C++ libraries specific to hardware.

## Native Method Libraries

Collection of C/C++ Native Libraries necessary to the Execution Engine and accessed through JNI.

# Points to highlight 

1. Java is considered as both interpreted and compiled Language.
2. By design, Java is slower than compiled languages due to dynamic linking and run-time interpreting. JIT compiler compensate this disadvantages for repeating operations by keeping a native code instead of bytecode.
3. The latest Java versions address performance bottlenecks in its original architecture.
4. JVM is only a specification so the implementation depends on vendors (innovation, performance improvements...)

# Java Memory Model

JVM resides in memory and consumes OS memory space like another application.

![JVM memory](./images/JVM_memory.png)

JVM stores runtime data and compiled code in a memory area composed by:

* Heap, 
* Non-Heap,
* Cache.

## Heap Memory

Heap is divided into 2 parts:
* Young Generation
* Old Generation

Heap is allocated when JVM starts up (Initial size is `-Xms`), its size increases or decreases while the application is running and its maximum size is `-Xmx`.

![Heap memory](./images/Heap_Memory.png)

### Young Generation

This area is reserved to newly-allocated objects and includes 3 parts:
* Eden memory
* 2 survivor spaces (S0, S1)

Most newly-created objects goes to Eden space. When Eden space is full a minor GC (Young Collection) is performed and all the survivor objects are moved to the survivor spaces.

Minor GC also checks the survivor objects and move them to the other survivor space. So at a time, one of the survivor space is always empty.

Objects that are survived after many cycles of GC, are moved to the Old generation memory space. Usually it is done by setting a threshold for the age of the young generation objects before they become eligible to promote to Old generation.

### Old Generation

This is reserved for containing long lived objects that could survive after many rounds of Minor GC.
When Old Gen space is full, Major GC (Old Collection) is performed (usually takes longer time)

## Non-Heap Memory

This area includes:

* **Permanent Generation** (Replaced by **Metaspace** since Java 8): contains the application metadata required by the JVM to describe the classes and methods used in the application
* **Method Area**: part of space in the Permanent Generation and used to store class structure (runtime constants and static variables) and code for methods and constructors.
* **Memory Pools** are created by JVM memory managers to create a pool of immutable objects if the implementation supports it. String Pool is a good example of this kind of memory pool. Memory Pool can belong to Heap or Perm Gen, depending on the JVM memory manager implementation.<br>It stores per-class structures such as runtime constant pool, field and method data, and the code for methods and constructors, as well as interned Strings.
* **Runtime constant pool** is per-class runtime representation of constant pool in a class. It contains class runtime constants and static methods. Runtime constant pool is part of the method area.

Permanent Generation (PG) is populated by JVM at runtime based on the classes used by the application. PG also contains Java SE library classes and methods. PG objects are garbage collected in a full garbage collection.

Its size can be changed using `-XX:PermSize` and `-XX:MaxPermSize`.

![Non Heap memory](./images/NonHeap_Memory.png)

## Cache Memory

This area includes Code Cache. It stores compiled code (i.e. native code) generated by JIT compiler, JVM internal structures, loaded profiler agent code and data, etc.

When Code Cache exceeds a threshold, it gets flushed (and objects are not relocated by the GC).

## Main cache memory options

* Codecache Size Options:

|Option|Default|Description|
|------|-------|-----------|
|`-XX:InitialCodeCacheSize=<value>`|160K (varies)|Initial code cache size (in bytes)|
|`-XX:ReservedCodeCacheSize=<value>`|32M/48M|Reserved code cache size (in bytes) - maximum code cache size|
|`-XX:CodeCacheExpansionSize=<value>`|32K/64K|Code cache expansion size (in bytes)|

* Diagnostic Options:

|Option|Default|Description|
|------|-------|-----------|
|`-XX:+PrintFlagsFinal`|false|Print all JVM options after argument and ergonomic processing|
|`-XX:+PrintCodeCache`|false|Print the code cache memory usage when exiting|
|`-XX:+PrintCodeCacheOnCompilation`|false|Print the code cache memory usage each time a method is compiled|

You can get information on codecache usage by specifying `–XX:+PrintCodeCache` on the java launcher command line. When your application exits, you will see output similar to the following:

```log
CodeCache: size=32768Kb used=542Kb max_used=542Kb free=32226Kb
 bounds [0xb414a000, 0xb41d2000, 0xb614a000] 
 total_blobs=131 nmethods=5 adapters=63 
 compilation: enabled
```
The most useful part of the output for codecache reduction efforts is the first line. The following describes each of the values printed:

* **size**: The maximum size of the codecache. It should be equivalent to what was specified by `–XX:ReservedCodeCacheSize`. Note that this is not the actual amount of physical memory (RAM) used by the codecache. This is just the amount of virtual address space set aside for it.

* **used**: The amount of codecache memory actually in use. This is usually the amount of RAM the codecache occupies. However, due to fragmentation and the intermixing of free and allocated blocks of memory within the codecache, it is possible that the codecache occupies more RAM than is indicated by this value, because blocks that were used then freed are likely still in RAM.

* **max_used**: This is the high water mark for codecache usage; the maximum size that the codecache has grown to and used. This generally is considered to be the amount of RAM occupied by the codecache, and will include any free memory in the codecache that was at some point in use. For this reason, it is the number you will likely want to use when determining how much codecache your application is using.

* **free**: This is size minus used.

The `–XX:+PrintCodeCacheOnCompilation` option also produces the same output as the first line above produced by `–XX:+PrintCodeCache`, but does so each time a method is compiled. It can be useful for measuring applications that do not terminate.

For more information, see [Oracle documentation](https://docs.oracle.com/javase/8/embedded/develop-apps-platforms/codecache.htm).


# Memory related issues

For critical memory issues, he JVM gets crashed and throws an error indication in your program output like:

* `java.lang.StackOverflowError`: Thread requires a larger stack than allowed (Stack Memory is full)
*  `java.lang.OutOfMemoryError`: Java heap space — indicates that Heap Memory is full
* `java.lang.OutOfMemoryError`: GC Overhead limit exceeded — indicates that GC has reached its overhead limit
* `java.lang.OutOfMemoryError`: Permgen space — indicates that Permanent Generation space is full
* `java.lang.OutOfMemoryError`: Metaspace — indicates that Metaspace is full (since Java 8)
* `java.lang.OutOfMemoryError`: Unable to create new native thread — indicates that JVM native code can no longer create a new native thread from the underlying operating system because so many threads have been already created and they consume all the available memory for the JVM
* `java.lang.OutOfMemoryError`: request size bytes for reason — indicates that swap memory space is fully consumed by application
* `java.lang.OutOfMemoryError`: Requested array size exceeds VM limit– indicates that our application uses an array size more than the allowed size for the underlying platform

> **Important notice**
> 
> These outputs can only indicate the impact that the JVM had, not the actual error: the root cause conditions can occur somewhere in your code (e.g. memory leak, GC issue, synchronization problem), resource allocation, or maybe even hardware setting

# Java options focused on memory

|Option|	Description|
|------|---------------|
|`-Xms`| Sets the initial heap size when JVM starts|
|`-Xmx`| Sets the maximum heap size.|
|`-Xmn`| Sets the size of the Young Generation, rest of the space goes for Old Generation.|
|`-XX:PermGen`|	Sets the initial size of the Permanent Generation memory|
|`-XX:MaxPermGen`|	Sets the maximum size of Perm Gen|
|`-XX:SurvivorRatio`| Provides ratio of Eden space and Survivor Space, for example if Young Generation size is 10m and VM switch is `-XX:SurvivorRatio=2` then 5m will be reserved for Eden Space and 2.5m each for both the Survivor spaces. The default value is 8.|
|`-XX:NewRatio`| Provides ratio of old/new generation sizes. The default value is 2.|
|`-XX:ReservedCodeCacheSize=size` | Sets the maximum code cache size (in bytes) for JIT-compiled code. This option has a limit of 2 GB; otherwise, an error is generated. The maximum code cache size should not be less than the initial code cache size.  This option is equivalent to `-Xmaxjitcodesize`|
|`-XX:InitialCodeCacheSize`|Initial code cache size (in bytes)|


# Exhaustive Java execution options

Here a review of interesting option for Java SE 8 and earlier versions.

* **Standard options** are guaranteed to be supported by all implementations of the Java Virtual Machine (JVM). They are used for common actions, such as checking the version of the JRE, setting the class path, enabling verbose output, and so on.

* **Non-standard options** are general purpose options that are specific to the Java HotSpot Virtual Machine, so they are not guaranteed to be supported by all JVM implementations, and are subject to change. These options start with `-X`.

* **Advanced options** are not recommended for casual use. These are developer options used for tuning specific areas of the Java HotSpot Virtual Machine operation that often have specific system requirements and may require privileged access to system configuration parameters. They are also not guaranteed to be supported by all JVM implementations, and are subject to change. Advanced options start with `-XX`.

Boolean options are used to either enable a feature that is disabled by default or disable a feature that is enabled by default. Such options do not require a parameter. Boolean `-XX` options are enabled using the plus sign (`-XX:+OptionName`) and disabled using the minus sign (`-XX:-OptionName`).

For options that require an argument, the argument may be separated from the option name by a space, a colon (`:`), or an equal sign (`=`), or the argument may directly follow the option (the exact syntax differs for each option). If you are expected to specify the size in bytes, you can use no suffix, or use the suffix k or K for kilobytes (KB), m or M for megabytes (MB), g or G for gigabytes (GB). For example, to set the size to 8 GB, you can specify either 8g, 8192m, 8388608k, or 8589934592 as the argument. If you are expected to specify the percentage, use a number from 0 to 1 (for example, specify 0.25 for 25%).

The way to provide the exhaustive list of options for a JVM is to tip the following command:

```
java -XX:+UnlockDiagnosticVMOptions -XX:+UnlockExperimentalVMOptions -XX:+PrintFlagsFinal -version
```

> **Oracle Hotspot specific parameter**
>
> In the OS environment where both 32 and 64-bit packages are installed, the JVM automatically chooses 32-bit environmental packages as default. If we want to set the environment to 64 bit manually, we can do so using `-d<OS bit>` parameter. And obviously, the OS bit can be either 32 or 64.
>
> Example: `java -d64 -server -XX:+AggressiveOpts -XX:+UseLargePages -Xmn10g  -Xms26g -Xmx26g`

Options are adapted to client or server application. Fine tuning requires first **jstat** or **VisualVM** analysis, GC logs or Heap dumps with history and usage in mind. Combining logs with **Apache JMeter** performance campaign is probably a good way to proceed.

> **jstat**
>
> `jstat` is included in the JDK, you can use it in command line.
> For executing `jstat` you need to know the process id of the application, you can get it easily using `ps -eaf | grep java` command.
> Once you have the Process ID of the java application you can run `jstat -gc <PID> <nb_of_samples_to_display>`.
> The advantage of jstat is that it can be executed in remote servers too where we don’t have GUI.
> * **S0C and S1C**: This column shows the current size of the Survivor0 and Survivor1 areas in KB.
> * **S0U and S1U**: This column shows the current usage of the Survivor0 and Survivor1 areas in KB. Notice that one of the survivor areas are empty all the time.
> * **EC and EU**: These columns show the current size and usage of Eden space in KB. Note that EU size is increasing and as soon as it crosses the EC, Minor GC is called and EU size is decreased.
> * **OC and OU**: These columns show the current size and current usage of Old generation in KB.
> * **PC and PU**: These columns show the current size and current usage of Perm Gen in KB.
> * **YGC and YGCT**: YGC column displays the number of GC event occurred in young generation. YGCT column displays the accumulated time for GC operations for Young generation. Notice that both of them are increased in the same row where EU value is dropped because of minor GC.
> * **FGC and FGCT**: FGC column displays the number of Full GC event occurred. FGCT column displays the accumulated time for Full GC operations. Notice that Full GC time is too high when compared to young generation GC timings.
> * **GCT**: This column displays the total accumulated time for GC operations. Notice that it’s sum of YGCT and FGCT column values.
>
> Full `jstat` parameters in https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jstat.html#BEHBBBDJ

> **visualvm**
>
> VisualVM is a graphical tool available now in http://visualvm.github.io/. To perform an interesting analysis, you must install the Visual GC plugin for having a visual view of Young and Old generations. 

In production mode, some open source projects such as **Apache Kafka** or **Postman** could help aggregating health data of your applications.

> **Java EE with Apache Tomcat and Apache Web server**
>
> If you deploy a Java EE application with Apache web server, enable Apache `mod_status` module that helps to monitor web server load and current httpd connections with an HTML interface that can be accessed via a web browser.<br>
Apache’s `mod_status` shows a plain HTML page containing the information about current statistics of the webserver including.
>
>* Total number of incoming requests
>* Total number of bytes and counts server
>* The CPU usage of Webserver
>* Server Load
>* Server Uptime
>* Total Traffic
>* Total number of idle workers
>* PIDs with the respective clients and many more.
>
> For more information read https://www.tecmint.com/monitor-apache-web-server-load-and-page-statistics/

If you can get Heap dump from your application, you can upload the result in https://heaphero.io/ for interesting reports.


## Behavioral options

Option    | Windows compliant  |Unix/Linux compliant | Description
----------|----------|----------|----------
`-XX:+AllowUserSignalHandlers` | Yes | Yes | Enables installation of signal handlers by the application. By default, this option is disabled and the application is not allowed to install signal handlers.
`-XX:+DisableExplicitGC` | Yes | Yes | By default calls to `System.gc()` are enabled (`-XX:-DisableExplicitGC`). Use `-XX:+DisableExplicitGC` to disable calls to `System.gc()`. Note that the JVM still performs garbage collection when necessary
`-XX:+FailOverToOldVerifier` | Yes | Yes | Enables automatic failover to the old verifier when the new type checker fails. By default, this option is disabled and it is ignored (that is, treated as disabled) for classes with a recent bytecode version. You can enable it for classes with older versions of the bytecode.
`-XX:+RelaxAccessControlCheck` | Yes | Yes | (Introduced in 6.) Decreases the amount of access control checks in the verifier. By default, this option is disabled, and it is ignored (that is, treated as disabled) for classes with a recent bytecode version. You can enable it for classes with older versions of the bytecode.
`-XX:+ScavengeBeforeFullGC` | Yes | Yes | Enables GC of the young generation before each full GC. This option is enabled by default. Oracle recommends that you do not disable it, because scavenging the young generation before a full GC can reduce the number of objects reachable from the old generation space into the young generation space. To disable GC of the young generation before each full GC, specify `-XX:-ScavengeBeforeFullGC`.
`-XX:+UseConcMarkSweepGC` | Yes | Yes | Enables the use of the CMS garbage collector for the old generation. Oracle recommends that you use the CMS garbage collector when application latency requirements cannot be met by the throughput (`-XX:+UseParallelGC`) garbage collector. The G1 garbage collector (`-XX:+UseG1GC`) is another alternative. By default, this option is disabled and the collector is chosen automatically based on the configuration of the machine and type of the JVM.<br>When this option is enabled, the `-XX:+UseParNewGC` option is automatically set and you should not disable it, because the following combination of options has been deprecated in JDK 8: `-XX:+UseConcMarkSweepGC -XX:-UseParNewGC`.
`-XX:+UseGCOverheadLimit` | Yes | Yes | Enables the use of a policy that limits the proportion of time spent by the JVM on GC before an `OutOfMemoryError` exception is thrown. This option is enabled, by default and the parallel GC will throw an `OutOfMemoryError` if more than 98% of the total time is spent on garbage collection and less than 2% of the heap is recovered. When the heap is small, this feature can be used to prevent applications from running for long periods of time with little or no progress. To disable this option, specify `-XX:-UseGCOverheadLimit`.
`-XX:+UseParallelGC` | Yes | Yes | Enables the use of the parallel scavenge garbage collector (also known as the throughput collector) to improve the performance of your application by leveraging multiple processors. By default, this option is disabled and the collector is chosen automatically based on the configuration of the machine and type of the JVM. If it is enabled, then the `-XX:+UseParallelOldGC` option is automatically enabled, unless you explicitly disable it.
`-XX:+UseParallelOldGC` | Yes | Yes | Enables the use of the parallel garbage collector for full GCs. By default, this option is disabled. Enabling it automatically enables the `-XX:+UseParallelGC` option.
`-XX:+UseSerialGC` | Yes | Yes | Enables the use of the serial garbage collector. This is generally the best choice for small and simple applications that do not require any special functionality from garbage collection. By default, this option is disabled and the collector is chosen automatically based on the configuration of the machine and type of the JVM.
`-XX:+UseTLAB` |Yes | Yes | Enables the use of thread-local allocation blocks (TLABs) in the young generation space. This option is enabled by default. To disable the use of TLABs, specify `-XX:-UseTLAB`.

## Garbage First (G1) Garbage Collection Options

Option    | Windows compliant  |Unix/Linux compliant | Description
----------|----------|----------|----------
`-XX:+UseG1GC` | Yes | Yes | Enables the use of the garbage-first (G1) garbage collector. It is a server-style garbage collector, targeted for multiprocessor machines with a large amount of RAM. It meets GC pause time goals with high probability, while maintaining good throughput. The G1 collector is recommended for applications requiring large heaps (sizes of around 6 GB or larger) with limited GC latency requirements (stable and predictable pause time below 0.5 seconds). By default, this option is disabled and the collector is chosen automatically based on the configuration of the machine and type of the JVM.
`-XX:MaxGCPauseMillis=time` | Yes | Yes | Sets a target for the maximum GC pause time (in milliseconds). This is a soft goal, and the JVM will make its best effort to achieve it. By default, there is no maximum pause time value. The following example shows how to set the maximum target pause time to 500 ms: `-XX:MaxGCPauseMillis=500`
`-XX:InitiatingHeapOccupancyPercent=percent` | Yes | Yes | Sets the percentage of the heap occupancy (0 to 100) at which to start a concurrent GC cycle. It is used by garbage collectors that trigger a concurrent GC cycle based on the occupancy of the entire heap, not just one of the generations (for example, the G1 garbage collector). By default, the initiating value is set to 45%. A value of 0 implies nonstop GC cycles. The following example shows how to set the initiating heap occupancy to 75%: `-XX:InitiatingHeapOccupancyPercent=75`
`-XX:NewRatio=ratio` | Yes | Yes | Sets the ratio between young and old generation sizes. By default, this option is set to 2. The following example shows how to set the young/old ratio to 1: `-XX:NewRatio=1`
`-XX:SurvivorRatio=ratio` | Yes | Yes | Sets the ratio between eden space size and survivor space size. By default, this option is set to 8. The following example shows how to set the eden/survivor space ratio to 4: `-XX:SurvivorRatio=4`
`-XX:MaxTenuringThreshold=threshold` | Yes | Yes | Sets the maximum tenuring threshold for use in adaptive GC sizing. The largest value is 15. The default value is 15 for the parallel (throughput) collector, and 6 for the CMS collector. The following example shows how to set the maximum tenuring threshold to 10:`-XX:MaxTenuringThreshold=10`
`-XX:ParallelGCThreads=threads` | Yes | Yes | Sets the number of threads used for parallel garbage collection in the young and old generations. The default value depends on the number of CPUs available to the JVM. For example, to set the number of threads for parallel GC to 2, specify the following option: `-XX:ParallelGCThreads=2`
`-XX:ConcGCThreads=threads` | Yes | Yes | Sets the number of threads used for concurrent GC. The default value depends on the number of CPUs available to the JVM. For example, to set the number of threads for concurrent GC to 2, specify the following option: `-XX:ConcGCThreads=2`
`-XX:G1ReservePercent=percent` | Yes | Yes | Sets the percentage of the heap (0 to 50) that is reserved as a false ceiling to reduce the possibility of promotion failure for the G1 collector. By default, this option is set to 10%. The following example shows how to set the reserved heap to 20%: `-XX:G1ReservePercent=20`
`-XX:G1HeapRegionSize=size` | Yes | Yes | Sets the size of the regions into which the Java heap is subdivided when using the garbage-first (G1) collector. The value can be between 1 MB and 32 MB. The default region size is determined ergonomically based on the heap size. The following example shows how to set the size of the subdivisions to 16 MB: `-XX:G1HeapRegionSize=16m`

## Performance Options

Option    | Windows compliant  |Unix/Linux compliant | Description
----------|----------|----------|----------
`-XX:+AggressiveOpts` | Yes | Yes | Enables the use of aggressive performance optimization features, which are expected to become default in upcoming releases. By default, this option is disabled and experimental performance features are not used.
`-XX:CompileThreshold=invocations` | Yes | Yes | Sets the number of interpreted method invocations before compilation. By default, in the server JVM, the JIT compiler performs 10,000 interpreted method invocations to gather information for efficient compilation. For the client JVM, the default setting is 1,500 invocations. This option is ignored when tiered compilation is enabled; see the option `-XX:+TieredCompilation`. The following example shows how to set the number of interpreted method invocations to 5,000: `-XX:CompileThreshold=5000`. You can completely disable interpretation of Java methods before compilation by specifying the `-Xcomp` option.
`-XX:MaxHeapFreeRatio=percent` | Yes | Yes | Sets the maximum allowed percentage of free heap space (0 to 100) after a GC event. If free heap space expands above this value, then the heap will be shrunk. By default, this value is set to 70%. The following example shows how to set the maximum free heap ratio to 75%: `-XX:MaxHeapFreeRatio=75`
`-XX:MaxNewSize=size` | Yes | Yes | Sets the maximum size (in bytes) of the heap for the young generation (nursery). The default value is set ergonomically.
`-XX:MinHeapFreeRatio=percent` | Yes | Yes | Sets the minimum allowed percentage of free heap space (0 to 100) after a GC event. If free heap space falls below this value, then the heap will be expanded.<br>By default, this value is set to 40%. The following example shows how to set the minimum free heap ratio to 25%: `-XX:MinHeapFreeRatio=25`
`-XX:NewRatio=ratio` | Yes | Yes | Sets the ratio between young and old generation sizes. By default, this option is set to 2. The following example shows how to set the young/old ratio to 1: `-XX:NewRatio=1`
`-XX:NewSize=size` | Yes | Yes | The `-XX:NewSize` option is equivalent to `-Xmn`. Sets the initial size (in bytes) of the heap for the young generation (nursery). Append the letter k or K to indicate kilobytes, m or M to indicate megabytes, g or G to indicate gigabytes.<br>The young generation region of the heap is used for new objects. GC is performed in this region more often than in other regions. If the size for the young generation is too low, then a large number of minor GCs will be performed. If the size is too high, then only full GCs will be performed, which can take a long time to complete.<br>Oracle recommends that you keep the size for the young generation between a half and a quarter of the overall heap size.
`-XX:ReservedCodeCacheSize=size` | Yes | Yes | Sets the maximum code cache size (in bytes) for JIT-compiled code. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes, g or G to indicate gigabytes.<br>The default maximum code cache size is 240 MB; if you disable tiered compilation with the option `-XX:-TieredCompilation`, then the default size is 48 MB. This option has a limit of 2 GB; otherwise, an error is generated. The maximum code cache size should not be less than the initial code cache size; see the option `-XX:InitialCodeCacheSize`. This option is equivalent to `-Xmaxjitcodesize`.
`-XX:InitialSurvivorRatio=ratio` | Yes | Yes | Sets the initial survivor space ratio used by the throughput garbage collector (which is enabled by the `-XX:+UseParallelGC` and/or `-XX:+UseParallelOldGC` options). Adaptive sizing is enabled by default with the throughput garbage collector by using the `-XX:+UseParallelGC` and `-XX:+UseParallelOldGC` options, and survivor space is resized according to the application behavior, starting with the initial value. If adaptive sizing is disabled (using the `-XX:-UseAdaptiveSizePolicy` option), then the `-XX:SurvivorRatio` option should be used to set the size of the survivor space for the entire execution of the application.<br>The following formula can be used to calculate the initial size of survivor space (S) based on the size of the young generation (Y), and the initial survivor space ratio (R): S=Y/(R+2). The 2 in the equation denotes two survivor spaces. The larger the value specified as the initial survivor space ratio, the smaller the initial survivor space size.<br>By default, the initial survivor space ratio is set to 8. If the default value for the young generation space size is used (2 MB), the initial size of the survivor space will be 0.2 MB. The following example shows how to set the initial survivor space ratio to 4: `-XX:InitialSurvivorRatio=4`
`-XX:TargetSurvivorRatio=percent` | Yes | Yes | Sets the desired percentage of survivor space (0 to 100) used after young garbage collection. By default, this option is set to 50%. The following example shows how to set the target survivor space ratio to 30%: `-XX:TargetSurvivorRatio=30`
`-XX:ThreadStackSize=size` | Yes | Yes | This option is equivalent to -Xss. The default value depends on virtual memory. Linux/ARM (32-bit): 320 KB, Linux/i386 (32-bit): 320 KB, Linux/x64 (64-bit): 1024 KB, OS X (64-bit): 1024 KB, Oracle Solaris/i386 (32-bit): 320 KB, Oracle Solaris/x64 (64-bit): 1024 KB. Example: `-XX:ThreadStackSize=1024k`
`-XX:-UseBiasedLocking` | Yes | Yes | Disables the use of biased locking. Some applications with significant amounts of uncontended synchronization may attain significant speedups with this flag enabled, whereas applications with certain patterns of locking may see slowdowns.<br>For more information about the biased locking technique, see the example in Java Tuning White Paper at http://www.oracle.com/technetwork/java/tuning-139912.html#section4.2.5.<br>By default, this option is enabled.
`-XX:+UseLargePages` | Yes | Yes | Enables the use of large page memory. By default, this option is disabled and large page memory is not used.  For more information, see [Large Pages](https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html#large_pages).
`-XX:AllocatePrefetchLines=lines` | Yes | Yes | Sets the number of cache lines to load after the last object allocation by using the prefetch instructions generated in compiled code. The default value is 1 if the last allocated object was an instance, and 3 if it was an array.<br>The following example shows how to set the number of loaded cache lines to 5: `-XX:AllocatePrefetchLines=5`.<br>Only the Java HotSpot Server VM supports this option.
`-XX:AllocatePrefetchStyle=style` | Yes | Yes | Sets the generated code style for prefetch instructions. The style argument is an integer from 0 to 3:<br>* 0 = Do not generate prefetch instructions,<br>* 1 = Execute prefetch instructions after each allocation. This is the default parameter,<br>* 2 = Use the thread-local allocation block (TLAB) watermark pointer to determine when prefetch instructions are executed,<br>3 = Use BIS instruction on SPARC for allocation prefetch.<br>Only the Java HotSpot Server VM supports this option.
`-XX:+OptimizeStringConcat`| Yes | Yes | Enables the optimization of String concatenation operations. This option is enabled by default. To disable the optimization of String concatenation operations, specify `-XX:-OptimizeStringConcat`.<br>Only the Java HotSpot Server VM supports this option.

## Debugging options

Option    | Windows compliant  |Unix/Linux compliant | Description
----------|----------|----------|----------
`-XX:ErrorFile=filename` | Yes | Yes | Specifies the path and file name to which error data is written when an irrecoverable error occurs. By default, this file is created in the current working directory and named `hs_err_pid<pid>.log` where `<pid>` is the identifier of the process that caused the error.<br>The following example shows how to set the default log file (note that the identifier of the process is specified as `%p`): `-XX:ErrorFile=./hs_err_pid%p.log`.<br>If the file cannot be created in the specified directory (due to insufficient space, permission problem, or another issue), then the file is created in the temporary directory for the operating system. 
`-XX:HeapDumpPath=path` | Yes | Yes | Sets the path and file name for writing the heap dump provided by the heap profiler (HPROF) when the `-XX:+HeapDumpOnOutOfMemoryError` option is set. By default, the file is created in the current working directory, and it is named `java_pid<pid>.hprof` where `<pid>` is the identifier of the process that caused the error.<br>The following example shows how to set the default file explicitly (%p represents the current process identificator): `-XX:HeapDumpPath=./java_pid%p.hprof`
`-XX:+HeapDumpOnOutOfMemoryError` | Yes | Yes | Enables the dumping of the Java heap to a file in the current directory by using the heap profiler (HPROF) when a `java.lang.OutOfMemoryError` exception is thrown. You can explicitly set the heap dump file path and name using the `-XX:HeapDumpPath` option. By default, this option is disabled and the heap is not dumped when an `OutOfMemoryError` exception is thrown.
`-XX:OnError=string` | Yes | Yes | Sets a custom command or a series of semicolon-separated commands to run when an irrecoverable error occurs. If the string contains spaces, then it must be enclosed in quotation marks.<br>The following example shows how the `-XX:OnError` option can be used to run the `gcore` command to create the core image, and the debugger is started to attach to the process in case of an irrecoverable error (the %p designates the current process): `-XX:OnError="gcore %p;dbx - %p"`
`-XX:OnOutOfMemoryError=string` | Yes | Yes | Sets a custom command or a series of semicolon-separated commands to run when an `OutOfMemoryError` exception is first thrown. If the string contains spaces, then it must be enclosed in quotation marks. For an example of a command string, see the description of the `-XX:OnError` option.
`-XX:OnOutOfMemoryError="<cmd args>; <cmd args>"` | Yes | Yes | Sets a custom command or a series of semicolon-separated commands to run when an `OutOfMemoryError` exception is first thrown. If the string contains spaces, then it must be enclosed in quotation marks. For an example of a command string, see the description of the `-XX:OnError` option.
`-XX:+PrintClassHistogram` | Yes | Yes | Enables printing of a class instance histogram after a Control+C event (SIGTERM). By default, this option is disabled.<br>Setting this option is equivalent to running the `jmap -histo` command, or the `jcmd pid GC.class_histogram` command, where pid is the current Java process identifier.
`-XX:+PrintConcurrentLocks` | Yes | Yes | Enables printing of java.util.concurrent locks after a Control+C event (SIGTERM). By default, this option is disabled.<br>Setting this option is equivalent to running the `jstack -l` command or the `jcmd pid Thread.print -l` command, where `pid` is the current Java process identifier.
`-XX:+PrintCommandLineFlags` | Yes | Yes | Enables printing of ergonomically selected JVM flags that appeared on the command line. It can be useful to know the ergonomic values set by the JVM, such as the heap space size and the selected garbage collector. By default, this option is disabled and flags are not printed.
`-XX:+PrintCompilation` | Yes | Yes | Enables verbose diagnostic output from the JVM by printing a message to the console every time a method is compiled. This enables you to see which methods actually get compiled. By default, this option is disabled and diagnostic output is not printed.<br>You can also log compilation activity to a file by using the -XX:+LogCompilation option.
`-XX:+PrintGC` | Yes | Yes | Enables printing of messages at every GC. By default, this option is disabled.
`-XX:+PrintGCDetails` | Yes | Yes | Enables printing of detailed messages at every GC. By default, this option is disabled.
`-XX:+PrintGCTimeStamps` | Yes | Yes | Enables printing of time stamps at every GC. By default, this option is disabled.
`-XX:+PrintTenuringDistribution` | Yes | Yes | Enables printing of tenuring age information. The following is an example of the output:<br>Desired survivor size 48286924 bytes, new threshold 10 (max 10)<br>- age 1: 28992024 bytes, 28992024 total<br>- age 2: 1366864 bytes, 30358888 total<br>- age 3: 1425912 bytes, 31784800 total<br>...<br>Age 1 objects are the youngest survivors (they were created after the previous scavenge, survived the latest scavenge, and moved from eden to survivor space). Age 2 objects have survived two scavenges (during the second scavenge they were copied from one survivor space to the next). And so on.<br>In the preceding example, 28 992 024 bytes survived one scavenge and were copied from eden to survivor space, 1 366 864 bytes are occupied by age 2 objects, etc. The third value in each row is the cumulative size of objects of age n or less.<br>By default, this option is disabled.
`-XX:+PrintAdaptiveSizePolicy`| Yes | Yes | Enables printing of information about adaptive generation sizing. By default, this option is disabled.
`-XX:+TraceClassLoading`| Yes | Yes | Enables tracing of classes as they are loaded. By default, this option is disabled and classes are not traced.
`-XX:+TraceClassLoadingPreorder` | Yes | Yes | Enables tracing of all loaded classes in the order in which they are referenced. By default, this option is disabled and classes are not traced.
`-XX:+TraceClassResolution` | Yes | Yes | Enables tracing of constant pool resolutions. By default, this option is disabled and constant pool resolutions are not traced.
`-XX:+TraceClassUnloading` | Yes | Yes | Enables tracing of classes as they are unloaded. By default, this option is disabled and classes are not traced.
`-XX:+TraceLoaderConstraints` | Yes | Yes |Enables tracing of the loader constraints recording. By default, this option is disabled and loader constraints recording is not traced.
`-XX:+PerfDataSaveToFile` | Yes | Yes | If enabled, saves jstat(1) binary data when the Java application exits. This binary data is saved in a file named `hsperfdata_<pid>`, where `<pid>` is the process identifier of the Java application you ran. Use jstat to display the performance data contained in this file as follows:<br>` jstat -class file:///<path>/hsperfdata_<pid>`<br>`jstat -gc file:///<path>/hsperfdata_<pid>`
`-XX:ParallelGCThreads=threads` | Yes | Yes | Sets the number of threads used for parallel garbage collection in the young and old generations. The default value depends on the number of CPUs available to the JVM.<br>For example, to set the number of threads for parallel GC to 2, specify the following option: `-XX:ParallelGCThreads=2`
`-XX:-UseCompressedOops` | Yes | Yes | Disables the use of compressed pointers. By default, this option is enabled, and compressed pointers are used when Java heap sizes are less than 32 GB. When this option is enabled, object references are represented as 32-bit offsets instead of 64-bit pointers, which typically increases performance when running the application with Java heap sizes less than 32 GB. This option works only for 64-bit JVMs.<br>It is also possible to use compressed pointers when Java heap sizes are greater than 32GB. See the `-XX:ObjectAlignmentInBytes` option.
`-XX:+AlwaysPreTouch` | Yes | Yes | Enables touching of every page on the Java heap during JVM initialization. This gets all pages into the memory before entering the `main()` method. The option can be used in testing to simulate a long-running system with all virtual memory mapped to physical memory. By default, this option is disabled and all pages are committed as JVM heap space fills.
`-XX:AllocatePrefetchDistance=size` | Yes | Yes | Sets the size (in bytes) of the prefetch distance for object allocation. Memory about to be written with the value of new objects is prefetched up to this distance starting from the address of the last allocated object. Each Java thread has its own allocation point.<br>Negative values denote that prefetch distance is chosen based on the platform. Positive values are bytes to prefetch. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes, g or G to indicate gigabytes. The default value is set to -1.<br>The following example shows how to set the prefetch distance to 1024 bytes:<br>`-XX:AllocatePrefetchDistance=1024`<br>Only the Java HotSpot Server VM supports this option.
`-XX:InlineSmallCode=size` | Yes | Yes | Sets the maximum code size (in bytes) for compiled methods that should be inlined. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes, g or G to indicate gigabytes. Only compiled methods with the size smaller than the specified size will be inlined. By default, the maximum code size is set to 1000 bytes: `-XX:InlineSmallCode=1000`
`-XX:MaxInlineSize=size`| Yes | Yes | Sets the maximum bytecode size (in bytes) of a method to be inlined. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes, g or G to indicate gigabytes. By default, the maximum bytecode size is set to 35 bytes: `-XX:MaxInlineSize=35`
`-XX:MaxTenuringThreshold=threshold` | Yes | Yes | Sets the maximum tenuring threshold for use in adaptive GC sizing. The largest value is 15. The default value is 15 for the parallel (throughput) collector, and 6 for the CMS collector.<br>The following example shows how to set the maximum tenuring threshold to 10: `-XX:MaxTenuringThreshold=10`
`-Xloggc:filename` | Yes | Yes | Sets the file to which verbose GC events information should be redirected for logging. The information written to this file is similar to the output of `-verbose:gc` with the time elapsed since the first GC event preceding each logged event. The `-Xloggc` option overrides `-verbose:gc` if both are given with the same java command.<br>Example:<br>`-Xloggc:garbage-collection.log`


# Large Pages

Also known as huge pages, large pages are memory pages that are significantly larger than the standard memory page size (which varies depending on the processor and operating system). Large pages optimize processor Translation-Lookaside Buffers.

A Translation-Lookaside Buffer (TLB) is a page translation cache that holds the most-recently used virtual-to-physical address translations. TLB is a scarce system resource. A TLB miss can be costly as the processor must then read from the hierarchical page table, which may require multiple memory accesses. By using a larger memory page size, a single TLB entry can represent a larger memory range. There will be less pressure on TLB, and memory-intensive applications may have better performance.

However, large pages page memory can negatively affect system performance. For example, when a large mount of memory is pinned by an application, it may create a shortage of regular memory and cause excessive paging in other applications and slow down the entire system. Also, a system that has been up for a long time could produce excessive fragmentation, which could make it impossible to reserve enough large page memory. When this happens, either the OS or JVM reverts to using regular pages.

# Performance Tuning Examples

The following examples show how to use experimental tuning flags to either optimize throughput or to provide lower response time.

**Example 1 - Tuning for Higher Throughput**
```
    java -d64 -server -XX:+AggressiveOpts -XX:+UseLargePages -Xmn10g  -Xms26g -Xmx26g
```
**Example 2 - Tuning for Lower Response Time**
```
    java -d64 -XX:+UseG1GC -Xms26g Xmx26g -XX:MaxGCPauseMillis=500 -XX:+PrintGCTimeStamp
```

# Going deeper...

See additional subjects such as:
* Large Pages: https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html#large_pages
* Application Class Data Sharing (AppCDS): https://docs.oracle.com/javase/8/docs/technotes/guides/vm/class-data-sharing.html with `-Xshareclasses` option (https://www.ibm.com/support/knowledgecenter/fr/SSYKE2_7.0.0/com.ibm.java.lnx.70.doc/diag/appendixes/cmdline/Xshareclasses.html & https://github.com/eclipse/openj9-docs/blob/master/docs/xshareclasses.md)
* Interesting article about memory management vs application performance: https://blogs.oracle.com/poonam/can-young-generation-size-impact-the-application-response-times

***

Web sources:

* https://medium.com/platform-engineer/understanding-jvm-architecture-22c0ddf09722
* https://medium.com/platform-engineer/understanding-java-memory-model-1d0863f6d973
* https://medium.com/platform-engineer/understanding-java-garbage-collection-54fc9230659a

* https://www.journaldev.com/2856/java-jvm-memory-model-memory-management-in-java
* https://www.journaldev.com/4098/java-heap-space-vs-stack-memory

* https://docs.oracle.com/javase/8/docs/technotes/tools/unix/java.html
* https://blogs.oracle.com/poonam/why-do-i-get-message-codecache-is-full-compiler-has-been-disabled
* https://docs.oracle.com/javase/8/docs/technotes/guides/vm/server-class.html
* https://docs.oracle.com/en/java/javase/11/vm/java-virtual-machine-technology-overview.html
* https://docs.oracle.com/javase/8/embedded/develop-apps-platforms/codecache.htm
* https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html#CBBIJCHG
* https://www.oracle.com/java/technologies/javase/vmoptions-jsp.html
* https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html
* https://docs.oracle.com/javase/8/docs/technotes/tools/unix/java.html
* https://docs.oracle.com/javase/8/docs/technotes/guides/extensions/spec.html
* https://docs.oracle.com/javase/7/docs/technotes/guides/vm/G1.html
* https://www.oracle.com/technical-resources/articles/java/g1gc.html

* https://www.baeldung.com/java-stack-heap

* https://geekflare.com/important-jvm-options/

* https://www.ibm.com/support/knowledgecenter/fr/SSYKE2_7.0.0/com.ibm.java.lnx.70.doc/diag/appendixes/cmdline/commands_jvm.html

Books sources:
* Java concurrency in Practice by Brian Goetz (Addison-Wesley Professional)
* Core Java 2, Volume 2 - Advanced features by Cay S. Hortsmann & Gary Cornell (Pearson Education)
* Programmer en Java by Claude Delannoy, 11e Edition (Eyrolles)
* Java 9 concurrency cookbook by Javier Fernández González (Packt Edition)
* Java 9 Cookbook by Mohamed Sanaulla, Nick Samoylov (Packt Edition)