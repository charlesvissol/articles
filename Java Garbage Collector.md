![](/home/vissol/Documents/Personal Articles/angrybee_Garbage_Collector.png)

*Follow me on Github: https://github.com/charlesvissol*

# Introduction

The JVM and the GC have a default behavior-based self-tuning to improve application performance. This process of self-tuning is known as **Egonomics**. But self-tuning is based on common behaviors that could not fit with some contexts & usages.

Java application performance tuning requires a fine understanding of how Java memory works and how Garbage Collector (GC) runs in background.

Java memory management is the process of allocating new objects and removing unused objects  (Garbage Collections) properly and automatically. Not like in C or C++ where programmers need to perform manually Garbage Collection. In Java a GC works in the background to clean up the  unused/unreferenced objects and free up some memory.

**Each JVM can implement its own version of garbage collection**. However, it should meet the standard JVM specification of working with the  objects present in the heap memory, marking or identifying the  unreachable objects, and destroying them with compaction.

To get an exhaustive view of your JVM flags tip the following command:

```bash
java -XX:+UnlockDiagnosticVMOptions -XX:+UnlockExperimentalVMOptions -XX:+PrintFlagsFinal -version
```

NB: `:=` in flags output indicates that the flag was set to that value either by the user or by JVM optimization.



> **Important information**
>
> You can find the exhaustive list of main JVM in https://chriswhocodes.com/vm-options-explorer.html
>
> You can also compare options between JVM versions in https://chriswhocodes.com/hotspot_option_differences.html

# Definition of a Garbage Collector

Java applications have 2 categories of objects:

- *Live objects* - these objects are being used and referenced from somewhere else
- *Dead objects* - these objects are no longer used or referenced from anywhere

The garbage collector finds these unused objects and deletes them to free up memory. 

The main objective of GC is to free heap memory by destroying the objects that don’t contain a reference. When there are no references to an object, it is assumed to be *dead* and no longer needed. So the memory occupied by the object can be reclaimed.

In details, a GC performs the following operations:

1. Allocates from and gives back memory to the operating system.
2. Hands out that memory to the application as it requests it.
3. Determines which parts of that memory is still in use by the application.
4. Reclaims the unused memory for reuse by the application.

>  **Definition**
>
> An object is considered garbage and its memory can be reused by the JVM when it can **no longer be reached** from any reference of any other live object in the running program.

# Eligibility for Garbage Collection

An object is said to be eligible for GC if it is unreachable. An unreachable object doesn’t contain any reference to it or is part of island of isolation.

The ways to make an object eligible for GC are:

  1. Nullifying the reference variable
  2. Re-assigning the reference variable
  3. Object created inside method
  4. Island of Isolation

Example of nullifying the reference variable:

```java
Student student = new Student();
student = null;
```
Example of re-assigning the reference variable:
```java
Student studentOne = new Student();
Student studentTwo = new Student();
studentOne = studentTwo; // now the first object referred by studentOne is available for garbage collection
```
Example of object created inside method (anonymous object):
```java
register(new Student());
```

Island of isolation: Basically, an island of isolation is a group of objects that reference each other but they are not referenced by any active object in the application. Strictly speaking, even a single unreferenced object is an island of isolation too. 

Example:

```java
public class Test 
{ 
	Test i; 
	public static void main(String[] args) 
	{ 
		Test t1 = new Test(); 
		Test t2 = new Test(); 
		
		// Object of t1 gets a copy of t2 
		t1.i = t2; 
	
		// Object of t2 gets a copy of t1 
		t2.i = t1; 
		
		// Till now no object eligible 
		// for garbage collection 
		t1 = null; 
		
		//now two objects are eligible for 
		// garbage collection 
		t2 = null; 
		
		// calling garbage collector 
		System.gc(); 
		
	} 

	@Override
	protected void finalize() throws Throwable 
	{ 
		System.out.println("Finalize method called"); 
	} 
} 
```

Runtime execution output:

```bash
Finalize method called
Finalize method called
```

Before destructing an object, Garbage Collector calls finalize method at most one time on that object.
The reason finalize method called two times in above example because two objects are eligible for garbage collection. This is because we don’t have any external references to `t1` and `t2` objects after executing ` t2=null`.
All we have is only internal references (which is in instance variable `i` of class `Test`) to them of each other. There is no way we can call instance variable of both objects. So, none of the objects can be called again.

# The importance of the Garbage Collector

The Garbage Collector offers a useful support to free the application developer from manual dynamic memory management. This completely eliminates some classes of errors related to memory management. 

> **Important notice**
>
> For small applications, GC activity is negligible regarding throughput issues. However, for large systems GC may become principal bottlenecks.<br>
> However, small improvements in reducing such a bottleneck can produce large gains in performance. For a sufficiently large system, it becomes worthwhile to select the right garbage collector and to tune it if necessary.

The Java HotSpot VM provides a selection of garbage collection algorithms to fit to application throughput with, if necessary, tuning for best performance.

The basic applications can perform well in the presence of garbage collection with pauses of modest frequency and duration. However, this isn't the case for a large class of applications, particularly those with large amounts of data (multiple gigabytes), many threads, and high transaction rates.

In the Java platform, there are currently **height supported garbage collection alternatives** and all but one of them, the **serial GC**, parallelize the work to improve performance. It's very important to keep the overhead of doing garbage collection as low as possible.

The serial collector is usually adequate for most small applications, in particular those requiring **heaps of up to approximately 100MB** on modern processors. However, for large and heavily threaded applications, managing a large amount of memory (up to 4GB) and 2 or more processors, the Garbage-First (G1) collector is selected by default until Java 8.

# How JVM & GC work?

The JVM provides **platform-dependent default selections** for the garbage collector, heap size, and runtime compiler.

These selections match the needs of most common applications. However, behavior-based tuning dynamically optimizes the sizes of the heap to meet a specified behavior of the application.

## Garbage Collection Roots

### Marking Reachable Objects

Every modern GC algorithm used in JVM starts its job with finding out all objects that are still alive. This concept is best explained using  the following picture representing your JVM’s memory layout:

![GC Roots](/home/vissol/Documents/Personal Articles/images/GCRoots.png)

First, GC defines some specific objects as **Garbage Collection Roots**. Examples of such GC roots are:

- Local variable and input parameters of the currently executing methods
- Active threads
- Static field of the loaded classes
- JNI references

Next, GC traverses the whole object graph in memory, starting from those Garbage Collection Roots and following references from the roots to other objects, e.g. instance fields. Every object the GC visits is **marked** as alive.

When the marking phase finishes, every live object is marked. All other objects (grey data structures on the picture above) are thus unreachable from  the GC roots, implying that the application cannot use the unreachable objects anymore. Such objects are considered garbage and GC should get rid of them in the following phases.

There are important aspects to note about the marking phase:

- The application threads need to be stopped for the marking to happen as you cannot really traverse the graph if it keeps changing under at the meantime. Such a situation when the application threads are temporarily stopped so that the JVM can indulge in housekeeping activities is called a **safe point** resulting in a **Stop The World pause**. Safe points can be triggered for different reasons but garbage collection is by far the most common reason for a safe point to be introduced.
- The duration of this pause depends neither on the total number of objects in heap nor on the size of the heap but on the number of **alive objects.** So increasing the size of the heap does not directly affect the duration of the marking phase.

When the **mark** phase is completed, the GC can proceed to the next step and start removing the unreachable objects.

### Removing unused objects

Removal of unused objects is somewhat different for different GC algorithms but all such GC algorithms can be divided into three groups:  sweeping, compacting and copying.

#### Sweep

**Mark and Sweep** algorithms use conceptually the simplest approach to garbage by just ignoring such objects. What this means is that after the marking phase has completed all space occupied by unvisited objects is considered free and can thus be reused to allocate new objects.

The approach requires using the so called **free-list** recording of every free region and its size. The management of the free-lists adds overhead to object allocation. Built into this approach is another weakness – there may exist plenty of free regions but if no single region is large enough to accommodate the allocation, the allocation is still going to fail (with an OutOfMemoryError).

![GC Sweep](/home/vissol/Documents/Personal Articles/images/GC-sweep.png)

#### Compact

**Mark-Sweep-Compact** algorithms solve the shortcomings of Mark and Sweep by moving all marked (and thus alive) objects to the beginning of the memory region. The downside of this approach is an increased GC pause duration as we need to copy all objects to a new place and to update all references to such objects. The benefits to Mark and Sweep are also visible – after such a compacting operation new object allocation is again extremely cheap. Using such approach the location of the free space is always known and no fragmentation issues are triggered either.

![GC Compact](/home/vissol/Documents/Personal Articles/images/GC-mark-sweep-compact.png)

#### Copy

**Mark and Copy** algorithms are very similar to the Mark and Compact as they too relocate all live objects. The important difference is that the target of relocation is a different memory region as a new home for survivors. Mark and Copy approach has some advantages as copying can occur simultaneously with marking during the same phase. The disadvantage is the need for one more memory region, which should be large enough to accommodate survived objects.

![GC Copy](/home/vissol/Documents/Personal Articles/images/GC-mark-and-copy-in-Java.png)

# Generational Garbage Collection strategy

GC **makes assumptions** about the way applications use objects.

The Java HotSpot VM incorporates a number of different garbage collection algorithms that all except ZGC use a technique called **generational collection**.

Generational collection technique exploits several empirically observed properties of most applications to minimize the work required to reclaim unused objects. The most important of these observed properties is the **weak generational hypothesis**, which states that most objects survive for only a short period of time. Some applications have very different looking distributions, but a surprisingly large number possess this general shape.

Typical lifetime distribution for objects:

![Objects lifetime](./images/objects_lifetime.png)

**Legend:**

* x-axis shows object lifetimes measured in bytes allocated
* y-axis is the total bytes in objects with the corresponding lifetime

**Explanation:**

* The sharp peak at the left represents objects that can be reclaimed (in other words, have "died") shortly after being allocated
* Some objects do live longer, and so the distribution stretches out to the right (there are typically some objects allocated at initialization that live until the VM exits)
* Between these two extremes are objects that live for the duration of some intermediate computation, seen here as the lump to the right of the initial peak

Most objects have a very short life as shown by the higher values on the left side of the graph. This is why Java categorizes objects into generations and performs garbage collection accordingly. Since the GC algorithms are optimized for objects which either "die young" or "are likely to live forever", the JVM behaves rather poorly  with objects with "medium" life expectancy.

The heap memory area in the JVM is divided into three sections:

![JVM regions](/home/vissol/Documents/Personal Articles/images/jvm_regions.png)

For any particular application, tunable parameters can be adjusted to improve GC (and application) performance.

## Young Generation

The Young Generation, when newly objects are created, is subdivided into:

- **Eden space** - all new objects start here, and initial memory is allocated to them
- **Survivor spaces (FromSpace and ToSpace)** - objects are moved here from Eden after surviving one garbage collection cycle. 

### Eden Space

As there are typically multiple threads creating a lot of objects simultaneously, Eden is further divided into one or more **Thread Local Allocation Buffer** (TLAB for short) residing in the Eden space. These buffers allow the JVM to allocate most objects within one thread directly in the corresponding TLAB, avoiding the expensive synchronization with other threads.

When allocation inside a TLAB is not possible (typically because  there’s not enough room there), the allocation moves on to a shared Eden space. If there’s not enough room in there either, a garbage collection process in Young Generation is triggered to free up more space. If the  garbage collection also does not result in sufficient free memory inside Eden, then the object is allocated in the Old Generation.

When Eden is being collected, GC walks all the reachable objects from the roots and marks them as alive.

![TLAB](/home/vissol/Documents/Personal Articles/images/TLAB-in-Eden-memory.png)

After the marking phase is completed, all the live objects in Eden are copied to one of the *Survivor spaces*. The whole Eden is now considered to be empty and can be reused to allocate more objects. Such an approach is called **“Mark and Copy”**: the live objects are marked, and then copied (not moved) to a survivor space.

### Survivor Space

Next to the Eden space reside two **Survivor** spaces called *from* and *to*. It is important to notice that one of the two Survivor spaces is always empty.

The empty Survivor space will start having residents next time the Young generation gets collected. All of the live objects from the whole of the Young generation (that includes both the Eden space and the non-empty "from" Survivor space) are copied to the "to" survivor space. After this process has completed, "to" now contains objects and "from" does not. Their roles are switched at this time.

![Survivor Spaces](/home/vissol/Documents/Personal Articles/images/how-java-garbage-collection-works.png)

This process of copying the live objects between the two Survivor spaces is repeated several times until some objects are considered to have matured and are old enough. Remember that, based on the generational hypothesis, objects which have survived for some time are expected to continue to be used for very long time.

Such tenured objects can thus be **promoted** to the Old Generation. When this happens, objects are not moved from one survivor space to another but instead to the Old space, where they will reside until they become unreachable.

To determine whether the object is old enough to be considered ready for propagation to Old space, GC tracks the number of collections a particular object has survived. After each generation of objects finishes with a GC, those still alive have their age incremented. Whenever the age exceeds a certain **tenuring threshold** the object will be promoted to Old space.

The actual tenuring threshold is dynamically adjusted by the JVM, but specifying `-XX:MaxTenuringThreshold=<value>` sets an upper limit on it. Setting `-XX:MaxTenuringThreshold=0` results in immediate promotion without copying it between Survivor spaces. By default, this threshold on modern JVMs is set to 15 GC cycles. This is also the maximum value in HotSpot.

Promotion may also happen prematurely if the size of the Survivor space is not enough to hold all of the live objects in the Young Generation.

When objects are garbage collected from the Young Generation, it is a **minor garbage collection event**. 

At any time, one of the survivor spaces is always empty. When the surviving objects reach a certain threshold of moving around the survivor spaces, they are moved to the Old Generation.

Use the `-Xmn<value>m` flag to set the size of the Young Generation.

## Old Generation

Objects that are long-lived are eventually moved from the Young Generation to the Old Generation. This is also known as **Tenured Generation**, and contains objects that have remained in the survivor spaces for a long  time. 

The implementation for the Old Generation memory space is much more complex. Old Generation is usually significantly larger and is occupied by objects that are less likely to be garbage.

GC in the Old Generation happens less frequently than in the Young  Generation. Also, since most objects are expected to be alive in the Old Generation, there is no Mark and Copy happening. Instead, the objects are moved around to minimize fragmentation. The algorithms cleaning the  Old space are generally built on different foundations. In principle, the steps are the following:

- Mark reachable objects by setting the marked bit next to all objects accessible through GC roots,
- Delete all unreachable objects,
- Compact the content of old space by copying the live objects contiguously to the beginning of the Old space.

GC in Old Generation has to deal with explicit compacting to avoid excessive fragmentation.

When objects are garbage collected from the Old Generation, it is a **major garbage collection event**.

Use the `-Xms<value>m` and `-Xmx<value>m` flags to set the size of the initial and maximum size of the Heap memory.

## Permanent Generation

Prior Java 8, Permanent Generation stores metadata: classes and methods and internalized strings. It caused lots of troubles for developers and OutOfMemoryError. The way to fix this problem was to simply increase the permgen size with java flag `-XX:MaxPermGen=<value>` defining the maximum size of Permanent Generation.

Use the `-XX:PermGen=<value>` and `-XX:MaxPermGen=<Value>` flags to set the initial and maximum size of the Permanent Generation.

## MetaSpace

Permanent Generation was removed in Java 8 in favor of the MetaSpace. The implementation differs from the PermGen and this space of the heap is now automatically resized.

The class definitions are now loaded into MetaSpace. It is located in the native memory and does not interfere  with the regular heap objects. By default, MetaSpace size is only limited by the amount of native memory available to the Java process.

This avoids the problem of applications running out of memory due to the limited size of the PermGen space of the heap. The MetaSpace memory can be garbage collected and the classes that are no longer used can be automatically cleaned when the MetaSpace reaches its maximum size.

To protect the application from OutOfMemoryError, use `-XX:MaxMetaSpaceSize=<value>`.

# Minor GC vs Major GC vs Full GC

The Garbage Collection events cleaning out different parts inside heap memory are often called Minor, Major and Full GC events. The Garbage Collection processes affect the throughput and the latency of application. Then, what is important about these events is whether they stopped the application and how long it took.

## Minor GC

Collecting garbage from the Young space is called **Minor GC**. Minor GC deals with following Minor Garbage Collection events:

1. Minor GC is always triggered when the JVM is unable to allocate space for a new object (Eden is getting full). So the higher the allocation rate, the more frequently Minor GC occurs.
2. During a Minor GC event, Tenured Generation is effectively ignored. References from Tenured Generation to Young Generation are considered to be GC roots. References from Young Generation to Tenured Generation are simply ignored during the mark phase.
3. Against common belief, Minor GC does trigger stop-the-world pauses, suspending the application threads. For most applications, the length of the pauses is negligible latency-wise if most of the objects in the Eden can be considered garbage and are never copied to Survivor/Old spaces. If the opposite is true and most of the newborn objects are not eligible for collection, Minor GC pauses start taking considerably more time.

**Minor GC cleans the Young Generation**.

## Major GC vs Full GC

There are no formal definitions for Major GC & Full GC. But on a first approach, building these definitions on top of what we know to be true about Minor GC cleaning Young space should be simple:

- **Major GC** is cleaning the Old space.
- **Full GC** is cleaning the entire Heap, both Young and Old spaces.

Unfortunately it is a bit more complex, but what is known:

* Many Major GCs are triggered by Minor GCs, so separating the two is impossible in many cases. 
* Modern garbage collection algorithms like G1 perform partial garbage cleaning so, using the term "cleaning" is only partially correct.

The important point is to focus on finding out whether the GC stops all the application threads or is able to progress concurrently with the application threads.

# GC causes

Following is a table which describes each possible GC Cause. It is ordered by an occurrence frequency in an average application.

| **GC Cause**                    | **Description**                                              |
| ------------------------------- | ------------------------------------------------------------ |
| Allocation Failure              | Application tried to make a new allocation and failed due to lack of available space in Young generation; hence Minor GC is required. On Linux, the JVM can trigger a GC if the kernel notifies there isn't much memory left via [mem_notify](http://lwn.net/Articles/267013/). |
| GC Locker                       | GC is started after all threads leave the JNI Critical region. For more information on JNI, refer to the [Java Native Interface documentation website](http://docs.oracle.com/javase/6/docs/technotes/guides/jni/index.html). GC is blocked when any thread is in the JNI Critical region and can start only when all of them outside of it. |
| G1 Evacuation Pause             | This is actual only for the G1 collector. It indicates that it is copying live objects from one set of regions (Young and sometimes Young + Tenured, which is called **Mixed**) to another set of regions. |
| G1 Humongous Allocation         | This is actual only for the G1 collector. Humongous is an allocation when its size is greater than 50% of one region size; the object then allocated in special space. Nevertheless, it also causes normal GC collection to get more (possibly continuous also) space for such objects. |
| CMS Initial Mark                | Initial mark phase of CMS. It also triggers Young Space collection. |
| System.gc()                     | There was a `System.gc()` call in the application code. You can start JVM with the `-XX:+DisableExplicitGC` flag to disable such behavior. |
| Adaptive Size Ergonomics        | Indicates you are using the adaptive heap size policy (ability to change Young and Tenured spaces size at runtime), enabled via the `-XX:+UseAdaptiveSizePolicy` flag. By default, it is enabled in the recent versions of JVM. |
| Allocation Profiler             | This is actual only for versions of Java before 8 and only when  `-Xaprof` is set. It triggers just before JVM exits. |
| Heap Inspection                 | GC was triggered by an inspection operation on the heap, most probably by the [jmap](http://docs.oracle.com/javase/7/docs/technotes/tools/share/jmap.html) tool with the `-histo:live` flag set. |
| Heap Dump                       | GC was initiated before heap dump is made by some profiling instrument. |
| No GC                           | Normally, you shouldn't see this reason. It was occurring in older Java versions, in case jstat command was started before any  collection occurred. Other case is when jstat checks GC without any GC  activity. |
| Last Ditch Collection           | When Metaspace (Java 8+) or PermGen (Java 7-) is full and you can't allocate a new object here, JVM first tries to clean it, triggering appropriate collector. If that is not possible, it then tries to expand it. If that doesn't work as well, it triggers Full GC with this cause name. Soft references are being cleaned during it as well. |
| Perm Generation Full            | Triggered as a result of an allocation failure in PermGen. Actual for Java versions prior to 8. |
| Metadata GC Threshold           | Triggered as a result of an allocation failure in Metaspace. Metaspace is a replacement for PermGen in Java 8+. |
| JvmtiEnv ForceGarbageCollection | Something called the JVM tool interface function ForceGarbageCollection. |

# Default GC values 

These are important garbage collector, heap size, and runtime compiler default selections: 

* Garbage-First (G1) Collector since Java 8
* The maximum number of GC threads is limited by heap size and available CPU resources 
* Initial heap size of 1/64 of physical memory (flag `-XX:InitialHeapSize=<value>`)
* Maximum heap size of 1/4 of physical memory  (flag `-XX:MaxHeapSize=<value>`)
* Tiered compiler, using both C1 and C2
* Server runtime compiler (`-server` option)

> **C1 & C2 compilers**
>
> C1 is JIT client compiler designed to run fast but producing a less optimized code regarding C2. 
>
> C2 is a JIT server compiler that takes a little more time to run but produces better-optimized code. C2 is better for long-running server applications that can spend more time on the compilation.
>
> During a normal program execution, Java uses both JIT compilers. A Java program, compiled with javac, starts its execution in an interpreted mode. The JVM tracks each frequently called method and compiles them. In order to do that, C1 is used for compilation. But Hotspot still keeps an eye on the future calls of those methods. If the number of calls increases, the JVM will recompile those methods once more, but using C2 this time. 
>
> This default strategy of the JVM is called **tiered compilation**.

# Garbage Collectors implementation


The Java Hotspot garbage collectors employ various techniques to improve the efficiency of these operations:

* Use **generational cleaning** in conjunction with aging to concentrate their efforts on areas in the heap that most likely contain a lot of reclaimable memory areas.
* Use multiple threads to aggressively make **operations parallel**, or perform some long-running operations in the background concurrent to the application.
* Try to recover larger contiguous free memory by **compacting live objects**.

Java Hotspot VM provides **multiple garbage collectors**, each designed to satisfy different requirements. 

Java SE selects the most appropriate garbage collector based on the class of the computer on which the application is run. However, this selection may not be optimal for every application that requires an explicit selection of the garbage collector and tune certain parameters to achieve the desired level of performance. 

The following list is a fast way to get yourself up to speed with which algorithm combinations are possible. Note that this stands true for Java 8, for older Java versions the available combinations might differ a  bit:

| **Young**             | **Tenured**      | **JVM options**                              |
| --------------------- | ---------------- | -------------------------------------------- |
| Incremental           | Incremental      | -Xincgc                                      |
| **Serial**            | **Serial**       | **-XX:+UseSerialGC**                         |
| Parallel Scavenge     | Serial           | -XX:+UseParallelGC -XX:-UseParallelOldGC     |
| Parallel New          | Serial           | N/A                                          |
| Serial                | Parallel Old     | N/A                                          |
| **Parallel Scavenge** | **Parallel Old** | **-XX:+UseParallelGC -XX:+UseParallelOldGC** |
| Parallel New          | Parallel Old     | N/A                                          |
| Serial                | CMS              | -XX:-UseParNewGC -XX:+UseConcMarkSweepGC     |
| Parallel Scavenge     | CMS              | N/A                                          |
| **Parallel New**      | **CMS**          | **-XX:+UseParNewGC -XX:+UseConcMarkSweepGC** |
| **G1**                |                  | **-XX:+UseG1GC**                             |
| **Epsilon GC**        |                  | **-XX:+UseEpsilonGC**                        |
| **Shenandoah**        |                  | **-XX:+UseShenandoahGC**                     |
| **ZGC**               |                  | **-XX:+UseZGC**                              |

The JVM has 8 type of Garbage Collectors.

## Serial GC

This is the simplest implementation of GC and is designed for small applications running on single-threaded environments. All garbage collection events are conducted serially in one thread. Compaction is executed after each garbage collection. 

Serial GC leads to a "stop the world" event where the entire application is paused. Since the entire application is frozen during  garbage collection, it is not recommended in a real world scenario where low latencies are required.

![Serial GC](/home/vissol/Documents/Personal Articles/images/SerialGC.png)

Call Serial GC with flag `-XX:+UseSerialGC`. 

## Parallel GC

The parallel collector is intended for applications with medium-sized to large-sized data sets that are run on multiprocessor or multi-threaded hardware. This is the default implementation of GC in the JVM and is also known as Throughput Collector. 

Multiple threads are used for minor garbage collection in the Young Generation. A single thread is used for major garbage collection in the Old Generation. 

Running the Parallel GC causes a "stop the world event" and the application freezes. Since it is more suitable in a multi-threaded environment, it can be used when a lot of work needs to be done and long pauses are acceptable, for example running a batch job.

The number of threads used during garbage collection is configurable via the command line parameter `-XX:ParallelGCThreads=<value>` . The default value is equal to the number of cores in your machine.

![Parallel GC](/home/vissol/Documents/Personal Articles/images/ParallelGC.png)

Parallel Garbage Collector is suitable on multi-core machines in cases where the primary goal is to increase throughput. Higher throughput is achieved due to more efficient usage of system resources:

- during collection, all cores are cleaning the garbage in parallel, resulting in shorter pauses
- between garbage collection cycles neither of the collectors is consuming any resources

On the other hand, as all phases of the collection have to happen without any interruptions, these collectors are still susceptible to long pauses during which application threads are stopped, so not compliant if latency is the primary goal.

Call Parallel GC with flag `-XX:+UseParallelGC`.

## Parallel Old GC

This is the default version of Parallel GC since **Java 7u4**. It is same as Parallel GC except that it uses multiple threads for both Young Generation and Old Generation. 

To use it, add flag `-XX:+UseParallelOldGC`.

## CMS (Concurrent Mark Sweep) GC

This is also known as the concurrent low pause collector. Multiple threads are used for minor garbage collection using the same algorithm as Parallel. Major garbage collection is multi-threaded, like Parallel Old GC, but CMS runs concurrently alongside application processes to minimize “stop the world” events. 

The CMS collector uses more CPU than other GCs. If you can allocate more CPU for better performance, then the CMS garbage collector is a better choice than the parallel collector but no compaction is performed in CMS GC.

Use `-XX:+UseConcMarkSweepGC` flag to enable CMS GC.

## G1 (Garbage First) GC

G1GC was intended as a replacement for CMS and was designed for multi-threaded applications that have a **large heap size** available (more than 4GB). It is parallel and concurrent like CMS, but it works quite differently compared to the older GCs.

G1 is a **generational**, it does not have separate regions for young and old generations. Instead, each generation is a set of regions, which allows resizing of the young generation in a flexible way.

It partitions the heap into a set of equal size regions (1MB to 32MB – depending on the size of the heap for a maximum of 2048 regions) and uses multiple threads to scan them. A region might be either an old region or a young region at any time during the program run.

First, the heap does not have to be split into contiguous Young and Old generation. Instead, the heap is split into a number (typically about 2048 with the 1MB to 32MB regions) smaller heap regions that can house objects. Each region may be an Eden region, a Survivor region or an Old region. The logical union of all Eden and Survivor regions is the Young Generation, and all the Old regions put together is the Old Generation:

![G1 regions](/home/vissol/Documents/Personal Articles/images/g1-011-591x187.png)

This allows the GC to avoid collecting the entire heap at once, and instead approach the problem incrementally: only a subset of the regions, called the **collection set** will be considered at a time. All the Young regions are collected during each pause, but some Old regions may be included as well:

![Collection set](/home/vissol/Documents/Personal Articles/images/g1-02-591x187.png)

Another novelty of G1 is that during the concurrent phase it estimates the amount of live data that each region contains. This is used in building the collection set: the regions that contain the most garbage are collected first. Hence the name: *garbage-first* collection.

After the mark phase is completed, G1 knows which regions contain the most garbage objects. If the user is interested in minimal pause times, G1 can choose to evacuate only a few regions. If the user is not worried about pause times or has stated a fairly large pause-time goal, G1 might choose to include more regions.

![G1GC](/home/vissol/Documents/Personal Articles/images/G1GC.png)

Apart from the Eden, Survivor, and Old memory regions, there are two more types of regions present in the G1GC:

* Humongous - used for large size objects (larger than 50% of heap size)
* Available - the unused or non-allocated space

One of the key design goals of G1 is to make the duration and distribution of stop-the-world pauses due to garbage collection predictable and configurable. In fact, Garbage-First is a *soft real-time* garbage collector, meaning that you can set specific performance goals to it. You can request the stop-the-world pauses to be no longer than x milliseconds within any given y millisecond long time range, (no  more than 5 milliseconds in any given second). Garbage-First GC will do its best to meet this goal with high probability (but not with certainty, that would be *hard real-time*).

Use `-XX:+UseG1GC` to enable G1GC.

### Evacuation Pause: Fully Young

In the beginning of the application’s lifecycle, G1 does not have any additional information from the not-yet-executed concurrent phases, so it initially functions in the fully-young mode. When the Young Generation fills up, the application threads are stopped, and the live data inside the Young regions is copied to Survivor regions, or any free regions that thereby become Survivor.

The process of copying these is called **Evacuation**, and it works in pretty much the same way as the other Young collectors we have seen before. The full logs of evacuation pauses are rather large, so, for simplicity’s sake we will leave out a couple of small bits that are irrelevant in the first fully-young evacuation pause. We will get back to them after the concurrent phases are explained in greater detail. In addition, due to the sheer size of the log record, the parallel phase details and “Other” phase details are extracted to separate sections:

```bash
0.134: [GC pause (G1 Evacuation Pause) (young), 0.0144119 secs]
    [Parallel Time: 13.9 ms, GC Workers: 8]
        …
    [Code Root Fixup: 0.0 ms]
    [Code Root Purge: 0.0 ms]
    [Clear CT: 0.1 ms]
    [Other: 0.4 ms]
        …
    [Eden: 24.0M(24.0M)->0.0B(13.0M) 8Survivors: 0.0B->3072.0K 9Heap: 24.0M(256.0M)->21.9M(256.0M)]
     [Times: user=0.04 sys=0.04, real=0.02 secs]
```

1. `0.134: [GC pause (G1 Evacuation Pause) (young), 0.0144119 secs]` G1 pause cleaning only (young) regions i.e. Eden
and Survivor regions. The pause started 134 ms after the JVM startup and the duration of the pause was 0.0144 seconds  measured in wall clock time.
2. `[Parallel Time: 13.9 ms, GC Workers: 8]` Indicating that for 13.9 ms (real time) the following activities were carried out by 8 threads in parallel.
3. `[Code Root Fixup: 0.0 ms]` Freeing up the data structures used for managing the parallel activities. Should always be near-zero. This is done sequentially.
4. `[Code Root Purge: 0.0 ms]` Cleaning up more data structures, should also be very fast, but non necessarily almost zero. This is done sequentially.
5. `[Other: 0.4 ms]` Miscellaneous other activities, many of which are also parallelized.
6. `[Eden: 24.0M(24.0M)->0.0B(13.0M)` Eden usage and capacity before and after the pause.
7. `Survivors: 0.0B->3072.0K` Space used by Survivor regions before and after the pause.
8. `Heap: 24.0M(256.0M)->21.9M(256.0M)]` Total heap usage and capacity before and after the pause.
9. `[Times: user=0.04 sys=0.04, real=0.02 secs]` Duration of the GC event, measured in different categories:

   * `user` Total CPU time that was consumed by Garbage Collector threads during this collection,

   * `sys` Time spent in OS calls or waiting for system event,

   * `real` Clock time for which your application was stopped. With the parallelizable activities during GC this number is ideally close to (user time + system time) divided by the number of threads used by Garbage Collector. In this particular case 8 threads were used. Note that due to some activities not being parallelizable, it always exceeds the ratio by a certain amount.

Most of the heavy-lifting is done by multiple dedicated GC worker threads. Their activities are described in the following section of the log. 

```bash
[Parallel Time: 13.9 ms, GC Workers: 8]
     [GC Worker Start (ms): Min: 134.0, Avg: 134.1, Max: 134.1, Diff: 0.1]
    [Ext Root Scanning (ms): Min: 0.1, Avg: 0.2, Max: 0.3, Diff: 0.2, Sum: 1.2]
    [Update RS (ms): Min: 0.0, Avg: 0.0, Max: 0.0, Diff: 0.0, Sum: 0.0]
        [Processed Buffers: Min: 0, Avg: 0.0, Max: 0, Diff: 0, Sum: 0]
    [Scan RS (ms): Min: 0.0, Avg: 0.0, Max: 0.0, Diff: 0.0, Sum: 0.0]
    [Code Root Scanning (ms): Min: 0.0, Avg: 0.0, Max: 0.2, Diff: 0.2, Sum: 0.2]
    [Object Copy (ms): Min: 10.8, Avg: 12.1, Max: 12.6, Diff: 1.9, Sum: 96.5]
    [Termination (ms): Min: 0.8, Avg: 1.5, Max: 2.8, Diff: 1.9, Sum: 12.2]
        [Termination Attempts: Min: 173, Avg: 293.2, Max: 362, Diff: 189, Sum: 2346]
    [GC Worker Other (ms): Min: 0.0, Avg: 0.0, Max: 0.0, Diff: 0.0, Sum: 0.1]
    GC Worker Total (ms): Min: 13.7, Avg: 13.8, Max: 13.8, Diff: 0.1, Sum: 110.2]
    [GC Worker End (ms): Min: 147.8, Avg: 147.8, Max: 147.8, Diff: 0.0] 
```

1. `[Parallel Time: 13.9 ms, GC Workers: 8]` Indicating that for 13.9 ms (clock time) the following activities were carried out by 8 threads in parallel
2. `[GC Worker Start (ms)` The moment in time at which the workers started their activity, matching the time stamp at the beginning of the pause. If Min and Max differ a lot, then it may be an indication that too many threads are  used or other processes on the machine are stealing CPU time from the  garbage collection process inside the JVM
3. `[Ext Root Scanning (ms)` How long it took to scan the external (non-heap) roots such as  class loaders, JNI references, JVM system roots, etc. Shows elapsed time, “Sum” is CPU time
4. `[Code Root Scanning (ms)` How long it took to scan the roots that came from the actual code: local vars, etc.
5. `[Object Copy (ms)` How long it took to copy the live objects away from the collected regions.
6. `[Termination (ms)` How long it took for the worker threads to ensure that they can safely stop and that there’s no more work to be done, and then actually terminate
7. `[Termination Attempts` How many attempts worker threads took to try and terminate. An attempt  is failed if the worker discovers that there’s in fact more work to be done, and it’s too early to terminate.
8. `[GC Worker Other (ms)` Other miscellaneous small activities that do not deserve a separate section in the logs.
9. `GC Worker Total (ms)` How long the worker threads have worked for in total
10. `[GC Worker End (ms)` The time stamp at which the workers have finished their jobs. Normally they should be roughly equal, otherwise it may be an indication of too many threads hanging around or a noisy neighbor

Additionally, there are some miscellaneous activities that are performed during the Evacuation pause. We will only cover a part of them in this  section. The rest will be covered later.

```bash
[Other: 0.4 ms]
    [Choose CSet: 0.0 ms]
    [Ref Proc: 0.2 ms]
    [Ref Enq: 0.0 ms]
    [Redirty Cards: 0.1 ms]
    [Humongous Register: 0.0 ms]
    [Humongous Reclaim: 0.0 ms]
    [Free CSet: 0.0 ms]
```

1. `[Other: 0.4 ms]` Miscellaneous other activities, many of which are also parallelized
2. `[Ref Proc: 0.2 ms]` The time it took to process non-strong references: clear them or determine that no clearing is needed.
3. `[Ref Enq: 0.0 ms]` The time it took to enqueue the remaining non-strong references to the appropriate `ReferenceQueue`
4. `[Free CSet: 0.0 ms]` The time it takes to return the freed regions in the collection set so that they are available for new allocations.

For more information, see [Oracle article](https://blogs.oracle.com/poonam/understanding-g1-gc-logs) dedicated to G1 GC logs.

### Concurrent Marking

G1 Concurrent Marking uses the Snapshot-At-The-Beginning approach that marks all the objects that were live at the beginning of the marking cycle, even if they have turned into garbage meanwhile. The information on which objects are live allows to build up the liveness stats for each region so that the collection set could be efficiently chosen afterwards.

This information is then used to perform garbage collection in the Old regions. It can happen fully concurrently, if the marking determines that a region contains only garbage, or during a stop-the-world evacuation pause for Old regions that contain both garbage and live objects.

Concurrent Marking starts when the overall occupancy of the heap is large enough. By default, it is 45%, but this can be changed by the `InitiatingHeapOccupancyPercent` JVM option. Like in CMS, Concurrent Marking in G1 consists of a number of phases, some of them fully concurrent, and some of them requiring the application threads to be stopped.



**Phase 1: Initial Mark**. 

This phase marks all the objects directly reachable from the GC roots. In CMS, it required a separate "stop the world" pause, but in G1 it is typically piggy-backed on an Evacuation Pause, so its overhead is minimal. You can notice this pause  in GC logs by the “(initial-mark)” addition in the first line of an  Evacuation Pause:

```bash
1.631: [GC pause (G1 Evacuation Pause) (young) (initial-mark), 0.0062656 secs]
```



**Phase 2: Root Region Scan.** 

This phase marks all the live objects reachable from the root regions (the ones that are not empty and that we might end up having to collect in the middle of the marking cycle). This phase has to complete before the next evacuation pause starts, because if it is interrupted, it can cause troubles. 

If it has to start earlier, it will request an early abort of root region scan, and then wait for it to finish. 

In the current implementation, the root regions are the survivor regions: they are the bits of Young Generation that will definitely be collected in the next Evacuation Pause.

```bash
1.362: [GC concurrent-root-region-scan-start]
1.364: [GC concurrent-root-region-scan-end, 0.0028513 secs]
```



**Phase 3. Concurrent Mark.** 

This phase is very much similar to CMS: it simply walks the object graph and marks the visited objects in a special bitmap. To ensure that the semantics of snapshot-at-the beginning are met, G1 GC requires that all the concurrent updates to the object graph made by the application threads leave the previous reference known for marking purposes. 

This is achieved by the use of the Pre-Write barriers (not to be confused with Post-Write barriers discussed later and memory barriers that relate to multi-threaded programming). Their function is to, whenever you write to a field while G1 Concurrent Marking is active, store the previous referee in the log buffers, to be processed by the concurrent marking threads.

```bash
1.364: [GC concurrent-mark-start]
1.645: [GC concurrent-mark-end, 0.2803470 secs]
```



**Phase 5. Cleanup.** 

This final phase prepares the ground for the upcoming evacuation phase, counting all the live objects in the heap regions, and sorting these regions by expected GC efficiency. It also performs all the house-keeping activities required to maintain the internal state for the next iteration of concurrent marking. 

Last but not least, the regions that contain no live objects at all are reclaimed in this phase. Some parts of this phase are concurrent, such as the empty region reclamation and most of the liveness calculation, but it also requires a short "stop the world" pause to finalize the picture while the application threads are not interfering. The logs for such "stop the world" pauses would be similar to:

```bash
1.652: [GC cleanup 1213M->1213M(1885M), 0.0030492 secs]
[Times: user=0.01 sys=0.00, real=0.00 secs]
```

In case when some heap regions that only contain garbage were discovered, the pause format can look a bit different, similar to:

```bash
1.872: [GC cleanup 1357M->173M(1996M), 0.0015664 secs]
[Times: user=0.01 sys=0.00, real=0.01 secs]
1.874: [GC concurrent-cleanup-start]
1.876: [GC concurrent-cleanup-end, 0.0014846 secs]
```

### Evacuation Pause: Mixed

After Concurrent Marking has successfully completed, G1 will schedule a mixed collection that will not only get the garbage away from the young regions, but also throw in a bunch of Old regions to the collection set.

A mixed Evacuation pause does not always immediately follow the end of the concurrent marking phase. There is a number of rules and heuristics that affect this. For instance, if it was possible to free up a large portion of the Old regions concurrently, then there is no need to do it.

There may, therefore, easily be a number of fully-young evacuation pauses between the end of concurrent marking and a mixed evacuation pause.

The exact number of Old regions to be added to the collection set, and the order in which they are added, is also selected based on a number of rules. These include the soft real-time performance goals specified for the application, the liveness and GC efficiency data collected during concurrent marking, and a number of configurable JVM options. The process of a mixed collection is largely the same as fully-young GC, but this time we will  also cover the subject of **remembered sets**.

Remembered sets are what allows the independent collection of different heap regions. For instance, when collecting region A,B and C, we have to know whether or not there are references to them from regions D and E to determine their liveness. But traversing the whole heap graph would take quite a while and ruin the whole point of incremental collection, therefore an optimization is employed. Much like we have the Card Table for independently collecting Young regions in other GC algorithms, we have **Remembered Sets** in G1.

As shown in the illustration below, each region has a Remembered Set that lists the references pointing to this region from the outside. These references will then be regarded as additional GC roots. Note that objects in Old regions that were determined to be garbage during concurrent marking will be ignored even if there are outside references to them: the referents are also garbage in that case.

![](/home/vissol/Documents/Personal Articles/images/g1-03-591x187.png)

What happens next is the same as what other collectors do: multiple parallel GC threads figure out what objects are live and which ones are  garbage:

![](/home/vissol/Documents/Personal Articles/images/g1-04-591x187.png)

And, finally, the live objects are moved to survivor regions, creating new if necessary. The now empty regions are freed and can be used for storing objects in again.

![](/home/vissol/Documents/Personal Articles/images/g1-05-v2-591x187.png)

To maintain the Remembered Sets, during the runtime of the application, a Post-Write Barrier is issued whenever a write to a field is performed. If the resulting reference is cross-region, i.e. pointing  from one region to another, a corresponding entry will appear in the Remembered Set of the target region. To reduce the overhead that the Write Barrier introduces, the process of putting the cards into the Remembered Set is asynchronous and features quite a number of optimizations. But basically it boils down to the Write Barrier putting  the dirty card information into a local buffer, and a specialized GC thread picking it up and propagating the information to the remembered  set of the referred region.

In the mixed mode, the logs publish certain new interesting aspects when compared to the fully young mode:

```bash
[Update RS (ms): Min: 0.7, Avg: 0.8, Max: 0.9, Diff: 0.2, Sum: 6.1]
[Processed Buffers: Min: 0, Avg: 2.2, Max: 5, Diff: 5, Sum: 18]
[Scan RS (ms): Min: 0.0, Avg: 0.1, Max: 0.2, Diff: 0.2, Sum: 0.8]
[Clear CT: 0.2 ms]
[Redirty Cards: 0.1 ms]
```

1. `[Update RS (ms)` Since the Remembered Sets are processed concurrently, we have to make sure that the still-buffered cards are processed before the actual collection begins. If this number is high, then the concurrent GC threads are unable to handle the load. It may be, e.g., because of an overwhelming number of incoming field modifications, or insufficient CPU resources.
2. `[Processed Buffers` How many local buffers each worker thread has processed.
3. `[Scan RS (ms)` How long it took to scan the references coming in from Remembered Sets.
4. `[Clear CT: 0.2 ms]` Time to clean the cards in the card table. Cleaning simply removes the “dirty” status that was put there to signify that a field was updated,  to be used for Remembered Sets.
5. `[Redirty Cards: 0.1 ms]` The time it takes to mark the appropriate locations in the card table as dirty. Appropriate locations are defined by the mutations to the  heap that GC does itself, e.g. while enqueuing references.

### Write barrier & Card Tables

In JVM, the memory space of objects is broken down into two spaces:

- Young generation (space): All new allocations (objects) are created inside this space.
- Old generation (space): This is where long lived objects exist (and probably die)

The idea is that, once an object survives a few garbage collection,  it is more likely to survive for a long time. So, objects that survive  garbage collection for more than a threshold, will be promoted to old  generation. The garbage collector runs more frequently in the young  generation and less frequently in the old generation. This is because  most objects live for a very short time.

We use generational garbage collection to avoid scanning of the whole memory space (like Mark and Sweep approach). In JVM, we have a **minor garbage collection** which is when GC runs inside the young generation and a **major garbage collection (or full GC)** which encompasses garbage collection of both young and old generations.

When doing minor garbage collection, JVM follows every reference from the live roots to the objects in the young generation, and marks those  objects as live, which excludes them from the garbage collection  process. The problem is that there may be some references from the  objects in the old generation to the objects in young generation, which  should be considered by GC, meaning those objects in young generation  that are referenced by objects in old generation should also be marked  as live and excluded from the garbage collection process.

One approach to solve this problem is to scan all of the objects in  the old generation and find their references to young objects. But this  approach is in contradiction with the idea of generational garbage  collectors. (Why we broke down our memory space into multiple  generations in the first place?)

Another approach is using **write barriers** and **card table**. When an  object in old generation writes/updates a reference to an object in the  young generation, this action goes through something called write  barrier. When JVM sees these write barriers, it updates the  corresponding entry in the card table. Card table is a table, which each one of its entries correspond to 512 bytes of the memory. You can think of it as an array containing `0` and `1` items. A `1` entry means there is an object in the corresponding area of the memory  which contains references to objects in young generation.

Now, when minor garbage collection is happening, first every  reference from the live roots to young objects are followed and the  referenced objects in young generation will be marked as live. Then,  instead of scanning all of the old object to find references to the  young objects, the card table is scanned. If GC finds any marked area in the card table, it loads the corresponding object and follows its  references to young objects and marks them as live either.

### G1 tunable options

* `-XX:G1HeapRegionSize=n`

Sets the size of a G1 region. The value will be a power of two and can range from 1MB to 32MB. The goal is to have around 2048  regions based on the minimum Java heap size.

* `-XX:MaxGCPauseMillis=200`

Sets a target value for desired maximum pause time. The  default value is 200 milliseconds. The specified value does not adapt to your heap size.

* `-XX:G1NewSizePercent=5`

Sets the percentage of the heap to use as the minimum for the young generation size. The default value is 5 percent of your Java heap. This is an experimental flag. This setting replaces the `-XX:DefaultMinNewGenPercent` setting. 

* `-XX:G1MaxNewSizePercent=60`

Sets the percentage of the heap size to use as the maximum for young generation size. The default value is 60 percent of your Java heap. This is an experimental flag. This setting replaces the `-XX:DefaultMaxNewGenPercent` setting. 

* `-XX:ParallelGCThreads=n`

Sets the number of threads used during parallel phases of the GC. Sets the value of n to the number of logical processors. The value of `n` is the same as the number of logical processors up to a value of 8.

If there are more than eight logical processors, sets the value of `n` to approximately 5/8 of the logical processors. This works in most cases except for larger SPARC systems where the value of `n` can be approximately 5/16 of the logical processors.

* `-XX:ConcGCThreads=n`

Sets the number of parallel marking threads. Sets `n` to approximately 1/4 of the number of parallel garbage collection threads (`ParallelGCThreads`).

* `-XX:InitiatingHeapOccupancyPercent=45`

Sets the Java heap occupancy threshold that triggers a marking cycle. The default occupancy is 45 percent of the entire Java  heap.

* `-XX:G1MixedGCLiveThresholdPercent=65`

Sets the occupancy threshold for an old region to be  included in a mixed garbage collection cycle. The default occupancy is 65 percent. This is an experimental flag. This setting replaces the `-XX:G1OldCSetRegionLiveThresholdPercent` setting.

* `-XX:G1HeapWastePercent=10`

Sets the percentage of heap that you are willing to waste. The Java HotSpot VM does not initiate the mixed garbage collection cycle when the reclaimable percentage is less than the heap waste percentage. The default is 10 percent.

* `-XX:G1MixedGCCountTarget=8`

Sets the target number of mixed garbage collections after a marking cycle to collect old regions with at most `G1MixedGCLIveThresholdPercent` live data. The default is 8 mixed garbage collections. The goal for mixed collections is to be within this target number. 

* `-XX:G1OldCSetRegionThresholdPercent=10`

Sets an upper limit on the number of old regions to be collected during a mixed garbage collection cycle. The default is 10 percent of the Java heap. This setting is not available in Java HotSpot  VM, build 23.

* `-XX:G1ReservePercent=10`

Sets the percentage of reserve memory to keep free so as  to reduce the risk of to-space overflows. The default is 10 percent.  When you increase or decrease the percentage, make sure to adjust the  total Java heap by the same amount. This setting is not available in  Java HotSpot VM, build 23.

### Recommendations

When you need to fine-tune G1 GC, keep the following recommendations in mind:

- **Young Generation Size**: Avoid explicitly setting young generation size with the `-Xmn` option or any or other related option such as `-XX:NewRatio`. Fixing the size of the young generation overrides the target pause-time goal.
- **Pause Time Goals**: When you evaluate or tune any garbage collection, there is always a latency versus throughput trade-off. The G1 GC is an incremental garbage collector with uniform  pauses, but also more overhead on the application threads. The throughput goal for the G1 GC is 90 percent application time and 10 percent garbage collection time. When you compare this to Java HotSpot VM's throughput collector, the goal there is 99 percent application time and 1 percent garbage collection time. Therefore, when you evaluate the G1 GC for throughput, **relax your pause-time target**. Setting too aggressive a goal indicates that you are willing to bear an increase in garbage collection overhead, which has a direct impact on throughput. When you evaluate the G1 GC for latency, you set your desired real-time goal, and the G1 GC will try to meet it. As a side effect,  throughput may suffer.
- **Taming Mixed Garbage Collections**: Experiment with the following options when you tune mixed garbage collections:
  - `-XX:InitiatingHeapOccupancyPercent` For changing the marking threshold.
  - `-XX:G1MixedGCLiveThresholdPercent` and `-XX:G1HeapWastePercent` When you want to change the mixed garbage collections decisions.
  - `-XX:G1MixedGCCountTarget` and `-XX:G1OldCSetRegionThresholdPercent` When you want to adjust the CSet for old regions.

### Overflow and Exhausted Log Messages

When you see to-space overflow/exhausted messages in your logs, the G1 GC does not have enough memory for either survivor or promoted  objects, or for both. The Java heap cannot expand since it is already at its max. Example messages:

```bash
924.897: [GC pause (G1 Evacuation Pause) (mixed) (to-space exhausted), 0.1957310 secs]
```

 OR

 ````bash
924.897: [GC pause (G1 Evacuation Pause) (mixed) (to-space overflow), 0.1957310 secs]
 ````

To alleviate the problem, try the following adjustments:

* Increase the value of the `-XX:G1ReservePercent` option (and the total heap accordingly) to increase the amount of reserve memory for "to-space".

* Start the marking cycle earlier by reducing the `-XX:InitiatingHeapOccupancyPercent`.

* You can also increase the value of the `-XX:ConcGCThreads` option to increase the number of parallel marking threads.

## Epsilon Garbage Collector

Epsilon is a do-nothing (no-op) garbage collector that was released as part of **JDK 11**. It handles memory allocation but does not implement any actual memory reclamation mechanism. Once the available Java heap is exhausted, the JVM shuts down.

It can be used for ultra-latency-sensitive applications, where developers know the application memory footprint exactly, or even have (almost) completely garbage-free applications. Usage of the Epsilon GC in any other scenario is otherwise discouraged.

Enable it with `-XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC ` flags.

## Shenandoah

Shenandoah is a new GC that was released as part of **JDK 12** and written by Red Hat. Shenandoah’s key advantage over G1 is that it does more of its garbage collection cycle work concurrently with the application threads. G1 can evacuate its heap regions only when the application is paused, while Shenandoah can relocate objects concurrently with the application (no "stop the world").

Shenandoah can compact live objects, clean garbage, and release RAM back to the OS almost immediately after detecting free memory. Since all of this happens concurrently while the application is running, Shenandoah is more CPU intensive.

Enable it with `-XX:+UnlockExperimentalVMOptions -XX:+UseShenandoahGC` flags.

## ZGC

ZGC is another GC written from scratch, that was released as part of **JDK 11** and has been improved in **JDK 12**. It is intended for applications which require low latency (less than 10 ms pauses) and/or use a very large heap (multi-TB).

The primary goals of ZGC are low latency, scalability, and ease of use. As a concurrent garbage collector, ZGC promises not to exceed application latency by 10 milliseconds, even for bigger heap sizes. It is also easy to tune. 

ZGC allows a Java application to continue running while it performs all garbage collection operations. By default, ZGC does not commits unused memory and returns it to the operating system.

For more details on ZGC, see https://wiki.openjdk.java.net/display/zgc.

> Both Shenandoah and ZGC are planned to be made production features and moved out of the experimental stage in JDK 15.

### ZGC origins

One of the features that resulted in the rise of Java in the early days was its automatic memory management with its GCs, which freed developers from manual memory management and lowered memory leaks. However, with unpredictable timings and durations, garbage collection can (at times)  do more harm to an application than good. Increased latency directly affects the throughput and performance of an application. With ever-decreasing hardware costs and programs engineered to use largish memories, applications are demanding lower latency and higher throughput from garbage collectors.

ZGC promises a latency of no more than 10 milliseconds, which doesn’t increase with heap size or a live set. This is because its "stop the world" pauses are limited to root scanning.

### Features of ZGC

ZGC brings in a lot of features, but one of the most outstanding features of ZGC is that it is a concurrent GC. Other features include:

- It can mark memory, copy and relocate it, all concurrently. It also has a concurrent reference processor.
- As opposed to the store barriers that are used by another HotSpot GCs, ZGC uses load barriers. The load barriers are used to keep track of heap usage.
- One of the highly interesting features of ZGC is the usage of load barriers with colored pointers. This is what enables ZGC to perform concurrent  operations when Java threads are running, such as object relocation or relocation set selection.
- ZGC is more flexible in configuring its size and scheme. Compared to G1, ZGC has better ways to deal with very large object allocations.
- ZGC is a single-generation GC. It also supports partial compaction. ZGC is also highly performant when it comes to reclaiming memory and  reallocating it.
- ZGC is NUMA-aware, which essentially means that it has a NUMA-aware memory allocator.

> The **NUMA-aware** architecture is a hardware design which separates its cores into multiple clusters where each cluster has its own local  memory region and still allows cores from one cluster to access all memory in the system.

The JVM arguments to use the ZGC are `-XX:+UnlockExperimentalVMOptions -XX:+UseZGC`.

Illustration of speed between ZGC, parallel GC and G1:

![ZGC](/home/vissol/Documents/Personal Articles/images/ZGC.jpg)



### ZGC heap

ZGC divides memory into regions, also called **ZPages**. ZPages can be dynamically created and destroyed. These can also be dynamically sized (unlike the G1 GC), which are multiples of 2 MB. Here are the size groups of heap regions:

- Small (2 MB)
- Medium (32 MB)
- Large (*N ** 2 MB)

ZGC heap can have multiple occurrences of these heap regions. The medium and large regions are allocated contiguously, as shown in the following diagram:

![ZGC heap regions](/home/vissol/Documents/Personal Articles/images/ZGC-heap-regions.png)

Unlike other GCs, the physical heap regions of ZGC can map into a bigger heap address space (which can include virtual memory). This can be crucial to fight memory fragmentation issues. Imagine that the user can allocate a really big object in memory, but can’t do so due to unavailability of contiguous space in memory... this often leads to multiple GC cycles to free up enough contiguous space. If none are available, even after (multiple) GC cycle(s), the JVM will shut down with `OutOfMemoryError`. 

However, this particular use case is not an issue with the ZGC. Since the physical memory maps to a bigger address space, locating a bigger contiguous space is feasible.

### ZGC phases

A GC cycle of ZGC includes multiple phases:

- Pause Mark Start
- Pause Mark End
- Pause Relocate Start

In the first phase, Pause Mark Start, ZGC marks objects that have been pointed to roots. This includes walking through the live set of objects, and then finding and marking them. This is by far one of the most heavy-duty workloads in the ZGC GC cycle.

Once this complete, the next cycle is Pause Mark End, which is used for synchronization and starts with a short pause of 1 ms. In this second phase, ZGC starts with reference processing and moves to week-root cleaning. It also includes the relocation set selection. ZGC marks the regions it wants to compact.

The next step, Pause Relocate Start, triggers the actual region compaction. It begins with root scanning pointing into the location set, followed by the concurrent reallocation of objects in the relocation set.

The first phase, that is, Pause Mark Start, also includes remapping the live data. Since marking and remap of live data is the most heavy-duty GC operation, it isn’t executed as a separate one. Remap starts after Pause Relocate Start but overlaps with the Pause Mark Start phase of the next GC cycle.

### Colored pointers

Colored pointers are one of the core concepts of ZGC. It enables ZGC to find, mark, locate, and remap the objects. It doesn’t support x32 platforms. Implementation of colored points needs virtual address masking, which could be accomplished either in the hardware, operating system, or software. The following diagram shows the 64-bit pointer layout:

<img src="/home/vissol/Documents/Personal Articles/images/colored-pointer.png" alt="Colored pointer" style="zoom:80%;" />

As shown in the preceding diagram, the 64-bit object reference is divided as follows:

- 18 bits: **Unused bits**
- 1-bit: **Finalizable**
- 1-bit: **Remapped**
- 1-bit: **Marked1**
- 1-bit: **Marked0**
- 42 bits: **Object Address**

The first 18 bits are reserved for future use. The 42 bits can address up to 4 TB of address space. Now comes the remaining the 4 bits: 

* The Marked1 and Marked0 bits are used to mark objects for garbage collection. 
* By setting the single bit for **Remapped**, an object can be marked not pointing to into the relocation set. 
* The last 1-bit for finalizing relates to concurrent reference processing. It marks that an object can only be reachable through a Finalizer.

When the user runs ZGC on a system, it will be notice that it uses a lot of virtual memory space, which is not the same as the physical memory space. This is due to heap multi-mapping. It specifies how the objects with the colored pointers are stored in the virtual memory.

### ZGC tunable options

To get the optimal performance, a heap size must be set up, that cannot only store the live set of your application but also has enough space to service the allocations.

ZGC is a concurrent garbage collector. By setting the amount of CPU time that should be assigned to ZGC threads, the user can control how  often the GC kicks in. It can be done so by using the following option:

```bash
-XX:ConcGCThreads=<number>
```

A higher value for the `ConcGCThreads` option will leave less amount of CPU time for your application. On the other hand, a lower value may result in your application struggling for memory: your application might generate more garbage than what is collected by ZGC.  ZGC can also use default values for `ConcGCThreads`. To fine-tune your application on this parameter, you might prefer to execute against test values.

For advanced ZGC tuning (these options are not part of Oracle GC Tuning guide...), the user can also enable large pages for enhanced performance of your application. It can be done by using the following option:

```bash
-XX:+UseLargePages
```

>  The preceding command needs root privileges. Please refer to https://wiki.openjdk.java.net/display/zgc#Main-EnablingLargePagesOnLinux for detailed steps.

Instead of enabling large pages (depending on you system), you can enable transparent huge pages by using the following option:

```bash
-XX:+UseLargePages -XX:+UseTransparentHugePages
```
Use of transparent huge pages is usually not recommended for latency sensitive applications, because it tends to cause unwanted latency spikes and using ZGC with transparent huge pages enabled requires kernel >= 4.7.

To enable transparent huge pages, configure the kernel (enable `madvise` mode):

```bash
$ echo madvise > /sys/kernel/mm/transparent_hugepage/enabled
$ echo advise > /sys/kernel/mm/transparent_hugepage/shmem_enabled
```

ZGC is a NUMA-aware GC. Applications executing on the NUMA machine enabled can result in a noticeable performance gain. By default, NUMA support is **enabled** for ZGC. However, if the JVM detects that it is bound to a sub-set of the CPUs in the system, this feature can be disabled. To override a JVM’s decision, the following option can be used:

```bash
-XX:+UseNUMA
```

# Enabling GC logs

There is a difference on the way to activate garbage collection logging for Java 8 and earlier and for the newer Java versions. Tuning GC logs through parameters is part of Java options at applications startup.

## Java 8 and earlier

For Java 8 and earlier you could add the following parameters to application startup:

```bash
java -XX:+PrintGCDetails -Xloggc:<PATH_TO_GC_LOG_FILE>
```

Example:


```bash
java -XX:+PrintGCDetails -Xloggc:$LOGBASEABS/logs/gc-'date +%F_%H-%M-%S'.log 
-XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=2M ...
```

NB: It is not necessary to include option `-XX:+PrintGCTimeStamps` because it is redundant with `-XX:+PrintGCDetails`.

When you enable GC logs, you need to enable rotation:

* `-XX:+UseGCLogFileRotation`: enable rotation
* `-XX:NumberOfGCLogFiles=5`: enable a maximum of 5 log files for rotation
* `-XX:GCLogFileSize=2M`: fix a maximum of 2MB GC log files

## Java 9 and newer

Java 9 introduced unified logging ([JEP 158](http://openjdk.java.net/jeps/158)), a central mechanism configurable with `-Xlog` to track class loading, threading, garbage collector, module system, etc.

The JVM-internal, unified logging infrastructure is very similar to known logging frameworks like Log4j or Logback that you might have used for your application. It generates textual messages, attaches some meta-information like tags (describing the originating subsystem), a log level (describing the  importance of the message), and time stamps before printing them  somewhere. The logging output can be configured according to your needs.

Logging can be activated with the `java` option `-Xlog`. This is *the only* flag regarding this mechanism - any further configuration is immediately appended to that option. Configurable aspects of logging are:

- which messages to log (by tag and/or by log level)
- which information to include (for example time stamps and process IDs)
- which output to use (for example a file)

First of all, you can grab a lot of information about JVM and its way of using Operating System by tipping the following command:

```bash
java -Xlog -version
```

Or

```bash
$ java -Xlog:help -version
```

By this command you display: glibc information, shared libraries, bootstrap, CodeHeap, page size, CPU number, CPU usage, etc.

#### Tags and log levels

The log level and tags can be used to define what exactly the logs should show by defining pairs `<tag-set>=<level>`, which are called *selectors*. All tags can be selected with `all` and the level is optional and defaults to `info`:

```bash
$ java -Xlog:all=warning -version
```

You can use also `debug` with tag set `logging`:

```bash
$ java -Xlog:logging=debug -version
```

> Available log levels are: `off`, `trace`, `debug`, `info`, `warning`, `error`

If you want to know more about default GC the JVM is using, tip:

```bash
java -Xlog:gc* -version
```

If you want GC and Heap information, tip:

```bash
$ java -Xlog:gc+heap* -version
```

```bash
$ java -Xlog:gc+heap* -version
[0.004s][info][gc,heap] Heap region size: 1M
[0.005s][info][gc,heap,coops] Heap address: 0x0000000707200000, size: 3982 MB, Compressed Oops mode: Zero based, Oop shift amount: 3
openjdk version "14.0.2" 2020-07-14
OpenJDK Runtime Environment (build 14.0.2+12-Ubuntu-120.04)
OpenJDK 64-Bit Server VM (build 14.0.2+12-Ubuntu-120.04, mixed mode, sharing)
[0,044s][info][gc,heap,exit ] Heap
[0,044s][info][gc,heap,exit ]  garbage-first heap   total 258048K, used 836K [0x0000000707200000, 0x0000000800000000)
[0,044s][info][gc,heap,exit ]   region size 1024K, 1 young (1024K), 0 survivors (0K)
[0,044s][info][gc,heap,exit ]  MetaSpace       used 114K, capacity 4480K, committed 4480K, reserved 1056768K
[0,044s][info][gc,heap,exit ]   class space    used 3K, capacity 384K, committed 384K, reserved 1048576K
```

You can also specify several <tag-set> separated by comma or joined with "+" (concatenate messages and contains exactly <tag-set> selected):

- Example with comma: `java -Xlog:gc*, heap* -version`
- Example with "+": `java -Xlog:gc,gc+heap -version`

> Wildcard "*" displays all messages levels. The easy way to proceed is to use it `-Xlog:<tag-set>*,<tag-set>*,<tag-set>*` and selectively switch to lower log levels when required. Example: `-Xlog:<tag-set>*=debug`.

> The <tag-set> values are: 
>
> `add, age, alloc, annotation, aot, arguments, attach, barrier, biasedlocking, blocks, bot, breakpoint, bytecode, cds, census, class, classhisto, cleanup, codecache, compaction, compilation, constantpool, constraints, container, coops, cpu, cset, data, datacreation, dcmd, decoder, defaultmethods, director, dump, dynamic, ergo, event, exceptions, exit, fingerprint, free, freelist, gc, handshake, hashtables, heap, humongous, ihop, iklass, init, inlining, install, interpreter, itables, jfr, jit, jni, jvmti, liveness, load, loader, logging, malloc, mark, marking, membername, memops, metadata, MetaSpace, methodcomparator, mirror, mmu, module, monitorinflation, monitormismatch, nestmates, nmethod, normalize, numa, objecttagging, obsolete, oldobject, oom, oopmap, oops, oopstorage, os, pagesize, parser, patch, path, perf, periodic, phases, plab, preorder, preview, promotion, protectiondomain, ptrqueue, purge, record, redefine, ref, refine, region, reloc, remset, resolve, safepoint, sampling, scavenge, setting, smr, stackmap, stacktrace, stackwalk, start, startuptime, state, stats, streaming, stringdedup, stringtable, subclass, survivor, sweep, symboltable, system, table, task, thread, time, timer, tlab, tracking, unload, unshareable, update, verification, verify, vmmutex, vmoperation, vmthread, vtables, vtablestubs, workgang`
>  Specifying `all` instead of a tag combination matches all tag combinations.

#### logs outputs

Compared to the non-trivial selectors, the output configuration is really simple. It comes after the selectors (separated by a colon) and has three possible locations:

- `stdout`: The default output. On the console that is the terminal window unless redirected, in IDEs it is often shown in a separate tab or view.
- `stderr`: The default error output. On the console that is the terminal window unless redirected, in IDEs it is usually shown in the same tab/view as `stdout` but printed red.
- `file=<filename>`: Defines a file to pipe all messages into. Putting in `file=` is optional.

Example: output in application.log file

```bash
$ java -Xlog:all=debug:file=application.log -version
```

In case, it is not possible to define more than one output option in a single `-Xlog` option, you can repeat the entire option with varied output options.

Example:

```bash
$ java -Xlog:all=debug:stdout -Xlog:all=debug:file=application.log -version
```

#### Decorators

Each log message consists of text and meta-information. Which of these additional pieces of information will be printed, is configurable by selecting *decorators*, which are listed in the following table.

| Option         | Description                                                  |
| -------------- | :----------------------------------------------------------- |
| `time`         | Current time and date in ISO-8601 format.                    |
| `uptime`       | Time since the start of the JVM in seconds and milliseconds (e.g., 6.567s). |
| `timemillis`   | The same value as generated by `System.currentTimeMillis()`. |
| `uptimemillis` | Milliseconds since the JVM started.                          |
| `timenanos`    | The same value as generated by `System.nanoTime()`.          |
| `uptimenanos`  | Nanoseconds since the JVM started.                           |
| `pid`          | The process identifier.                                      |
| `tid`          | The thread identifier.                                       |
| `level`        | The level associated with the log message.                   |
| `tags`         | The tag-set associated with the log message.                 |

Example: printing time stamp

```bash
$ java -Xlog:gc*=debug:stdout:time,uptimemillis,tid -version
[2021-02-25T07:53:58.380+0100][8ms][8471] Heap region size: 1M
[2021-02-25T07:53:58.380+0100][8ms][8471] Minimum heap 8388608  Initial heap 262144000  Maximum heap 4175429632
[2021-02-25T07:53:58.381+0100][9ms][8471] Heap address: 0x0000000707200000, size: 3982 MB, Compressed Oops mode: Zero based, Oop shift amount: 3
[2021-02-25T07:53:58.382+0100][10ms][8471] ConcGCThreads: 1 offset 8
[2021-02-25T07:53:58.382+0100][10ms][8471] ParallelGCThreads: 4
[2021-02-25T07:53:58.382+0100][10ms][8471] Initialize mark stack with 4096 chunks, maximum 16384
[2021-02-25T07:53:58.382+0100][10ms][8471] Expand the heap. requested expansion amount: 262144000B expansion amount: 262144000B
[2021-02-25T07:53:58.384+0100][12ms][8471] Target occupancy update: old: 0B, new: 262144000B
[2021-02-25T07:53:58.384+0100][12ms][8471] Initial Refinement Zones: green: 1024, yellow: 3072, red: 5120, min yellow size: 2048
[2021-02-25T07:53:58.385+0100][13ms][8471] Using G1

...

$ java -Xlog:gc*=debug:stdout -version
[0.006s][info][gc,heap] Heap region size: 1M
[0.006s][debug][gc,heap] Minimum heap 8388608  Initial heap 262144000  Maximum heap 4175429632
[0.006s][info ][gc,heap,coops] Heap address: 0x0000000707200000, size: 3982 MB, Compressed Oops mode: Zero based, Oop shift amount: 3
[0.007s][debug][gc           ] ConcGCThreads: 1 offset 8
[0.007s][debug][gc           ] ParallelGCThreads: 4
[0.007s][debug][gc           ] Initialize mark stack with 4096 chunks, maximum 16384
[0.007s][debug][gc,ergo,heap ] Expand the heap. requested expansion amount: 262144000B expansion amount: 262144000B
[0.010s][debug][gc,ihop      ] Target occupancy update: old: 0B, new: 262144000B
[0.010s][debug][gc,ergo,refine] Initial Refinement Zones: green: 1024, yellow: 3072, red: 5120, min yellow size: 2048
[0.011s][info ][gc            ] Using G1

...

```

#### Some examples 

```bash
java -Xlog:gc*:file=<PATH_TO_GC_LOG_FILE>
```

Example:

```bash
java -Xlog:gc*:file=/var/log/myapp/gc.log -jar my_awesome_app.jar
```

In this case, to enable the rotation, you add simply those parameters:

```bash
java -Xlog:gc*:file=/var/log/myapp/gc.log,filecount=5,filesize=5m
```

Another examples:

If you want to see all the available options for log levels, log decorators, and tag sets:

```bash
java -Xlog:logging=debug -version
```

If you want to log all GC messages to a file:

```bash
java -cp $CLASSPATH -Xlog:gc*=debug:file=/tmp/gc.log mypackage.MainClass
```

If you want to log all GC messages to both standard out and in a file:

```bash
java -cp $CLASSPATH -Xlog:gc*=debug:stdout -Xlog:gc*=debug:file=/tmp/gc.log mypackage.MainClass
```

# Tuning GC: theory

The two most important factors affecting garbage collection performance  are total available memory and proportion of the heap dedicated to the  young generation.

## Total Heap

The most important factor affecting garbage collection performance is total available memory. Because collections occur when generations fill up, throughput is inversely proportional to the amount of memory available.

At initialization of the virtual machine, the entire space for the heap is reserved. The size of the space reserved can be specified with the `-Xmx` option. If the value of the `-Xms` parameter is smaller than the value of the ` -Xmx` parameter, then not all of the space that's reserved is immediately committed to the virtual machine.

By default, the virtual machine grows or shrinks the heap at each collection to try to keep the proportion of free space to live objects at each collection within a specific range.

This target range is set as a percentage by the options `-XX:MinHeapFreeRatio=<minimum>` and `-XX:MaxHeapFreeRatio=<maximum>`, and the total size is bounded below by `–Xms<min>` and above by `–Xmx<max>`.                         

With these options, if the percent of free space in a generation falls below 40%, then the generation expands to maintain 40% free space, up to the maximum allowed size of the generation. Similarly, if the free space exceeds 70%, then the generation contracts so that only 70% of the space is free, subject to the minimum size of the generation.

 The calculation used in Java SE for the Parallel collector are now used for all the garbage collectors. 

## Conserving Dynamic Footprint by Minimizing Java Heap Size

If you need to minimize the dynamic memory footprint (the maximum RAM consumed during execution) for your application, then you can do this by minimizing the Java heap size. Java SE Embedded applications may require this...

Minimize Java heap size by lowering the values of the options` -XX:MaxHeapFreeRatio` (default value is 70%) and `-XX:MinHeapFreeRatio` (default value is 40%) with the command-line options `-XX:MaxHeapFreeRatio` and `-XX:MinHeapFreeRatio`. Lowering `-XX:MaxHeapFreeRatio` to as low as 10% and `-XX:MinHeapFreeRatio` has shown to successfully reduce the heap size without too much performance degradation. However, results may vary greatly depending on  your application. Try different values for these parameters until they are as low as possible, yet still retain acceptable performance.                     

In addition, you can specify `-XX:-ShrinkHeapInSteps`, which immediately reduces the Java heap to the target size (specified by the parameter `-XX:MaxHeapFreeRatio`). You may encounter performance degradation with this setting. By default, the Java runtime incrementally reduces the Java heap to the  target size; this process requires multiple garbage collection cycles.

## The Young Generation

After total available memory, the second most influential factor affecting garbage collection performance is the proportion of the heap dedicated to the young generation.

The bigger the young generation, the less often minor collections occur. However, for a bounded heap size, a larger young generation implies a smaller old generation, which will increase  the frequency of major collections. The optimal choice depends on the lifetime distribution of the objects allocated by the application.

### Young Generation Size Options

By default, the young generation size is controlled by the option `-XX:NewRatio`.

For example, setting `-XX:NewRatio=3` means that the ratio between the young and old generation is 1/3. In other words, the combined size of the eden and survivor spaces will be 1/4 of the total heap size.                        

The options `-XX:NewSize` and `-XX:MaxNewSize` bound the young generation size from below and above. Setting these to the same value fixes the young generation, just as setting `-Xms` and `-Xmx` to the same value fixes the total heap size. This is useful for tuning the young generation at a finer granularity than the integral multiples  allowed by `-XX:NewRatio`.                        

### Survivor Space Sizing

You can use the option `-XX:SurvivorRatio` to tune the size of the survivor spaces, but often this isn't important for performance.                     

For example, `-XX:SurvivorRatio=6` sets the ratio between eden and a survivor space to 1/6. In other words, each survivor space will be 1/6 of the size of eden, and thus 1/8 of the size of the young generation (not 1/7, because there are two survivor spaces).                        

If survivor spaces are too small, then the copying collection overflows directly into the old generation. If survivor spaces are too large, then they are uselessly empty. At each garbage collection, the virtual machine chooses a threshold number,  which is the number of times an object can be copied before it's old. This threshold is chosen to keep the survivors half full. You can use the log configuration `-Xlog:gc,age` to display this threshold and the age of objects in the new generation. It's also useful for observing the lifetime distribution of an application.

## Choose the right Garbage Collector

Unless your application has rather strict pause-time requirements, first run your application and allow the VM to select a collector. Most of the time, the default settings should work just fine.

If necessary, adjust the heap size to improve performance. If the performance still doesn't meet your goals, then use the following guidelines as a starting point for selecting a collector:

- If the application has a small data set (up to approximately 100 MB), then select the serial collector with the option `-XX:+UseSerialGC`.                        
- If the application will be run on a single processor and there are no pause-time requirements, then select the serial collector with the option `-XX:+UseSerialGC`.
- If (a) peak application performance is the first priority and (b) there are no pause-time requirements or pauses of one second or longer are acceptable, then let the VM select the collector or select the parallel collector with `-XX:+UseParallelGC`.
- If response time is more important than overall throughput and garbage collection pauses must be kept shorter, then select the                                                  mostly concurrent collector with `-XX:+UseG1GC`.
- If response time is a high priority, and/or you are using a very large heap, then select a fully concurrent collector with `-XX:+UnlockExperimentalVMOptions -XX:UseZGC`.

## Optimize your Garbage Collector

You can configure GC to preferentially meet one of two goals:

* maximum pause-time (latency)
* application throughput. 

If the preferred goal is met, the GC will try to maximize the other.

| Goal                             | Description                                                  |
| -------------------------------- | ------------------------------------------------------------ |
| **Maximum Pause-Time** (latency) | The pause time is the duration during which the GC stops the application and recovers space no longer in use. The intent of the maximum pause-time goal is to limit the duration of these pauses.<br>An average time for pauses and a variance on that average is maintained by the GC.<br>The maximum pause-time goal is specified with the command-line option: <br>`-XX:MaxGCPauseMillis=$value_in_ms`<br>The garbage collector adjusts the Java heap size and other parameters related to garbage collection in an attempt to keep garbage collection pauses shorter than `$value_in_ms`<br>The default for the maximum pause-time goal varies by collector. These adjustments may cause garbage collection to occur more frequently, reducing the overall throughput of the application. |
| **Throughput**                   | The throughput goal is measured in terms of the time spent collecting garbage, and the time spent outside of garbage collection is the application time.<br>The goal is specified by the command-line option:<br>`-XX:GCTimeRatio=nnn`. The ratio of garbage collection time to application time is 1/ (1+nnn).<br>For example, `-XX:GCTimeRatio=19` sets a goal of `1/20th` or `5%` of the total time for garbage collection. |

Consequences:

* Reducing Maximum Pause-Time Goal: Reducing the Pause-Time may cause GC to occur more frequently, reducing the overall throughput of the application. In some cases, the desired pause-time goal can't be met. 
* Elevating throughput Goal: The time spent in garbage collection is the total time for all garbage collection induced pauses. If the throughput goal isn't being met, then one possible action for the garbage collector is to increase the size of the heap so that the time spent in the application between collection pauses can be longer.

Users have different requirements of garbage collection. For example,  some consider the right metric for a web server to be throughput because pauses during garbage collection may be tolerable or simply obscured by network latencies. 

However, in an interactive graphics program, even short pauses may negatively affect the user experience.

On systems with limited physical memory or many processes, footprint may dictate scalability. Promptness is the time between when an object becomes dead and when the memory becomes available, an important consideration for distributed systems, including Remote Method Invocation (RMI).

In general, choosing the size for a particular generation is a trade-off between these considerations.

- A very large young generation may maximize throughput, but does so at the expense of footprint, promptness, and pause times
- Young generation pauses can be minimized by using a small young  generation at the expense of throughput. The sizing of one generation  doesn't affect the collection frequency and pause times for another generation.

> **Tuning principles**
>
> There is no one right way to choose the size of a generation. The best choice is determined by the way the application uses memory as well as  user requirements. Thus the virtual machine's choice of a garbage collector isn't always optimal and may be overridden with command-line  options.

## Define the right GC strategy

The heap size depends directly on the chosen throughput goal. Tuning the heap size to meet this goal means choosing a **maximum heap size**, and choosing **maximum pause-time** goal.

> **Rules**
>
> * Don't choose a maximum value for the heap unless you know that you need a heap greater than the default maximum heap size. 
> * Choose a throughput goal that's sufficient for your application.
> * Set the maximum heap size to a value that's close to the total physical memory on the platform, but doesn't cause swapping of the application
> * If the throughput goal can be met, but pauses are too long, then select a maximum pause-time goal. Choosing a maximum pause-time goal may mean that your throughput goal won't be met, so choose values that are an acceptable compromise for the application.

It's typical that the size of the heap oscillates as the garbage collector tries to satisfy competing goals. This is true even if the application has reached a steady state.

The pressure to achieve a throughput goal (which may require a larger heap) competes with the goals for a maximum pause-time and a minimum footprint (which both may require a small heap).

# Tuning GC: practice

**Allocation rate** is a term used when communicating the amount of memory allocated per time unit. Often it is expressed in MB/sec.

An excessively high allocation rate can mean trouble for your  application’s performance. When running on a JVM, the problem will be revealed by garbage collection posing a large overhead.

## Allocation Rate

### Measuring Allocation Rate

One way to measure the allocation rate is to turn on GC logging by specifying `-XX:+PrintGCDetails -XX:+PrintGCTimeStamps` flag for the JVM so the JVM starts logging the GC pauses similar to the following:

```bash
0.291: [GC (Allocation Failure) [PSYoungGen: 33280K->5088K(38400K)] 33280K->24360K(125952K), 0.0365286 secs] [Times: user=0.11 sys=0.02, real=0.04 secs] 
0.446: [GC (Allocation Failure) [PSYoungGen: 38368K->5120K(71680K)] 57640K->46240K(159232K), 0.0456796 secs] [Times: user=0.15 sys=0.02, real=0.04 secs] 
0.829: [GC (Allocation Failure) [PSYoungGen: 71680K->5120K(71680K)] 112800K->81912K(159232K), 0.0861795 secs] [Times: user=0.23 sys=0.03, real=0.09 secs]
```

From the GC log above, we can calculate the allocation rate as the difference between the sizes of the young generation after the completion of the last collection and before the start of the next one. Using the example above, we can extract the following information:

- *A*t **291** ms after the JVM was launched, **33 280 K** of objects were created. The first minor GC event cleaned the young generation, after which there were **5 088 K** of objects in the young generation left.
- At **446** ms after launch, the young generation occupancy had grown to **38 368 K**, triggering the next GC, which managed to reduce the young generation occupancy to **5 120 K**. Means an allocation of 38 368 - 5 088 = 33 280 K
- At **829** ms after the launch, the size of the young generation was **71 680 K** and the GC reduced it again to **5 120 K**. Means an allocation of 71 680 - 5 120 = 66 560 K.

This data can be expressed in the following table calculating the allocation rate as deltas of the young occupancy:

| **Event** | **Time** | **Young before** | **Young after** | **Allocated during** | **Allocation rate** |
| --------- | -------- | ---------------- | --------------- | -------------------- | ------------------- |
| 1st GC    | 291ms    | 33280KB          | 5088KB          | 33280KB              | **114MB/sec**       |
| 2nd GC    | 446ms    | 38368KB          | 5120KB          | 33280KB              | **215MB/sec**       |
| 3rd GC    | 829ms    | 71680KB          | 5120KB          | 66560KB              | **174MB/sec**       |
| Total     | 829ms    | N/A              | N/A             | 133120KB             | **161MB/sec**       |

This extract of log shows us that the allocation rate is **161 MB/sec** during the period of measurement.

### Objectives of the measures

These measures are dedicated to Allocation Rate, Young Generation & Eden.

Measuring the Allocation Rate is measuring the frequency of **Minor GC pauses** (cleaning the Young Generation). Allocation Rate covers Eden space. Allocation Rate impacts the throughput of the application (Allocation Rate VS Throughput)

Allocation takes place in Eden so sizing Eden impacts the Allocation Rate. The hypothesis is that increasing the size of Eden will reduce the frequency of minor GC pauses and thus allow the application to sustain faster allocation rates.

### Tuning Allocation Rate

When running the same application than with different Eden sizes using `-XX:NewSize -XX:MaxNewSize`   `-XX:SurvivorRatio` parameters, we can see a two-fold difference in allocation rates.

- Re-running with 100 MB of Eden reduces the allocation rate to below 100 MB/sec.
- Increasing Eden size to 1 GB increases the allocation rate to just below 200 MB/sec.

If you stop your application threads for GC less frequently, you  can do more useful work. More useful work also happens to create more  objects, thus supporting the increased allocation rate.

> **Important highlight**
>
> Allocation Rate might and probably does not directly correlate with the actual throughput of your application. 
>
> The allocation rate have an impact on how frequently your minor GC pauses stop application threads, but to see the overall impact, you also need to take into account **major GC pauses** and measure throughput not in MB/sec but in the business operations your application provides.

In some cases, when too much new objects are created, the frequency of minor GC pauses surges. Under enough of a load this can result in GC having a significant impact on throughput.

#### Demo application

Here a simple class to illustrate Allocation Rate tuning: 

```java
import java.util.concurrent.locks.LockSupport;

public class Boxing {

    private static volatile Double sensorValue;

    private static void readSensor() {
        while(true) {
            sensorValue = Math.random();
        }
    }

    private static void processSensorValue(Double value) {
        if(value != null) {
            // Be warned: may take more than one usec on some machines, especially Windows
            LockSupport.parkNanos(1000);
        }
    }

    public static void main(String[] args) {
        int iterations = args.length > 0 ? Integer.parseInt(args[0]) : 1_000_000;

        initSensor();

        for(int i = 0; i < iterations; i ++) {
            processSensorValue(sensorValue);
        }
    }

    private static void initSensor() {
        Thread sensorReader = new Thread(Boxing::readSensor);

        sensorReader.setDaemon(true);
        sensorReader.start();
    }
}
```

When running the application with flags  `-XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xmx32m` the GC logs displays:

```bash
2.808: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0003076 secs]
2.819: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0003079 secs]
2.830: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0002968 secs]
2.842: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0003374 secs]
2.853: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0004672 secs]
2.864: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0003371 secs]
2.875: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0003214 secs]
2.886: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0003374 secs]
2.896: [GC (Allocation Failure) [PSYoungGen: 9760K->32K(10240K)], 0.0003588 secs]
```

What should immediately grab attention is the **frequency of** **minor GC events**. This indicates that there are lots of objects being allocated. Additionally, the **post-GC occupancy of the young generation remains low, and no full collections are happening**. These symptoms indicate that the GC is having significant impact to the throughput of the application at hand.

#### Let's tune

In some cases, you can reduce a high allocation rate by increasing the the size of the young generation (`-XX:NewSize -XX:MaxNewSize`) but it won't reduce the allocation rate, it simply reduce the collections frequency. This approach is interesting when there is only few survivors every time because it impacts the number of surviving objects. See also the default values of the JVM to adapt the values.

In this case, you should simultaneously increase `Xmx` (in the demo application to `-Xmx64m`) to only decrease the frequency of minor GCs without modifying the number of survivors objects:

```bash
2.808: [GC (Allocation Failure) [PSYoungGen: 20512K->32K(20992K)], 0.0003748 secs]
2.831: [GC (Allocation Failure) [PSYoungGen: 20512K->32K(20992K)], 0.0004538 secs]
2.855: [GC (Allocation Failure) [PSYoungGen: 20512K->32K(20992K)], 0.0003355 secs]
2.879: [GC (Allocation Failure) [PSYoungGen: 20512K->32K(20992K)], 0.0005592 secs]
```

In our case, throwing more memory is not always the best solution. In fact, the program creates `Double` and we should replace it by primitive `double` to reduce the footprint of the application. This change reduce drastically the GC pauses.

In some cases, the JVM may be clever enough to remove excessive allocations itself by using the escape analysis technique. To make it short, the JIT compiler may in some cases prove that a created object never “escapes” the scope it is created in. In such cases, there is no actual need to allocate it on the heap and produce garbage this way, so the JIT compiler does just that: it eliminates the allocation.

## Premature Promotion

The concept of premature promotion must be first introduced by some explanations about **Promotion Rate**.

The promotion rate is measured in the amount of data propagated from the young generation to the old generation per time unit. It is often  measured in MB/sec, similarly to the allocation rate.

Promoting long-lived objects from the young generation to the old is how JVM is expected to behave. But if not only long-lived objects end up in the old generation. This situation can occur when objects with a short life expectancy are not collected in the young  generation and get promoted to the old generation, this is called **premature promotion**.

Cleaning these short-lived objects now becomes a job for major GC, which is not designed for frequent runs and results in longer GC pauses. This significantly affects the throughput of the application.

### Measuring Premature Promotion

To measure premature promotion, enable GC logs with `XX:+PrintGCDetails` and `-XX:+PrintGCTimeStamps` flags:

```bash
0.291: [GC (Allocation Failure) [PSYoungGen: 33280K->5088K(38400K)] 33280K->24360K(125952K), 0.0365286 secs] [Times: user=0.11 sys=0.02, real=0.04 secs] 
0.446: [GC (Allocation Failure) [PSYoungGen: 38368K->5120K(71680K)] 57640K->46240K(159232K), 0.0456796 secs] [Times: user=0.15 sys=0.02, real=0.04 secs] 
0.829: [GC (Allocation Failure) [PSYoungGen: 71680K->5120K(71680K)] 112800K->81912K(159232K), 0.0861795 secs] [Times: user=0.23 sys=0.03, real=0.09 secs]
```

From the above we can extract the size of the young Generation and the total heap both before and after the collection event. Knowing the  consumption of the young generation and the total heap, it is easy to calculate the consumption of the old generation (delta between the two).

Expressing the information in GC logs as:

| **Event** | **Time** | **Young decreased** | **Total decreased** | **Promoted** | **Promotion rate** |
| --------- | -------- | ------------------- | ------------------- | ------------ | ------------------ |
| 1st GC    | 291ms    | 28192K              | 8920K               | 19272K       | **66.2 MB/sec**    |
| 2nd GC    | 446ms    | 33248K              | 11400K              | 21848K       | **140.95 MB/sec**  |
| 3rd GC    | 829ms    | 66560K              | 30888K              | 35672K       | **93.14 MB/sec**   |
| Total     | 829ms    |                     |                     | 76792K       | **92.63 MB/sec**   |

We can see that on average the promotion rate is 92 MB/sec, peaking at 140.95 MB/sec for a while.

Notice that you can extract this information only from minor GC pauses. Full GC pauses do not expose the promotion rate as the change in the old generation usage in GC logs also includes objects cleaned by the major GC.

### Objectives of the measure

The main impact of the promotion rate is the change of frequency in GC  pauses. But as opposed to the allocation rate that affects the frequency of minor GC events, the promotion rate affects the frequency of major GC events.

The more stuff you promote to the old generation the faster you will fill it up. Filling the old generation faster means that the frequency of the GC events cleaning the old generation will increase and full garbage collections typically require much more time.

### Tuning Premature Promotion

The symptoms of premature promotion can take any of the following forms:

- The application goes through frequent full GC runs over a short period of time.
- The old generation consumption after each full GC is low, often under 10-20% of the total size of the old generation.
- Facing the promotion rate approaching the allocation rate.

#### Demo application

Here the demo application.

```java
import java.util.ArrayList;
import java.util.Collection;

public class PrematurePromotion {

    // This example shows how objects lingering in the heap for too long
    // may result in many more Full GC pauses than there could be.

    // 1. Run with: -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are many Full GCs
    //
    // 2. Run with: -verbose:gc -Xmx64m -XX:NewSize=32m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that most of GCs are minor
    //
    // 3. Run with: -Dmax.chunks=1000 -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that most of GCs are minor

    private static final int MAX_CHUNKS = Integer.getInteger("max.chunks", 10_000);

    private static final Collection<byte[]> accumulatedChunks = new ArrayList<>();

    private static void onNewChunk(byte[] bytes) {
        accumulatedChunks.add(bytes);

        if(accumulatedChunks.size() > MAX_CHUNKS) {
            processBatch(accumulatedChunks);
            accumulatedChunks.clear();
        }
    }

    public static void main(String[] args) {
        while(true) {
            onNewChunk(produceChunk());
        }
    }

    private static byte[] produceChunk() {
        byte[] bytes = new byte[1024];

        for(int i = 0; i < bytes.length; i ++) {
            bytes[i] = (byte) (Math.random() * Byte.MAX_VALUE);
        }

        return bytes;
    }

    public static volatile byte sink;

    public static void processBatch(Collection<byte[]> bytes) {
        byte result = 0;

        for(byte[] chunk : bytes) {
            for(byte b : chunk) {
                result ^= b;
            }
        }

        sink = result;
    }
}
```

We simulate here a premature promotion by making the objects tenure to the old generation a bit earlier than it happens by default. If we run the demo with a specific set of GC  parameters (`-Xmx24m -XX:NewSize=16m -XX:MaxTenuringThreshold=1`), we would see this in the garbage collection logs:

```bash
2.176: [Full GC (Ergonomics) [PSYoungGen: 9216K->0K(10752K)] [ParOldGen: 10020K->9042K(12288K)] 19236K->9042K(23040K), 0.0036840 secs]
2.394: [Full GC (Ergonomics) [PSYoungGen: 9216K->0K(10752K)] [ParOldGen: 9042K->8064K(12288K)] 18258K->8064K(23040K), 0.0032855 secs]
2.611: [Full GC (Ergonomics) [PSYoungGen: 9216K->0K(10752K)] [ParOldGen: 8064K->7085K(12288K)] 17280K->7085K(23040K), 0.0031675 secs]
2.817: [Full GC (Ergonomics) [PSYoungGen: 9216K->0K(10752K)] [ParOldGen: 7085K->6107K(12288K)] 16301K->6107K(23040K), 0.0030652 secs]
```

At first glance it may seem that premature promotion is not the issue here. Indeed, the occupancy of the old generation seems to be  decreasing on each cycle. However, if few or no objects were promoted, we would not be seeing a lot of full garbage collections.

There is a simple explanation for this GC behavior: while many objects are being promoted to the old generation, some existing objects are collected. This gives the impression that the old generation usage is decreasing, while in fact, there are objects that are constantly being promoted, triggering full GC.

#### Let's tune

There is 2 approaches to avoid premature promotion:

1. Increase the young generation size by using `-Xmx64m -XX:NewSize=32m` flags to fit to the young generation size and lower the Full GC frequency

```bash
2.251: [GC (Allocation Failure) [PSYoungGen: 28672K->3872K(28672K)] 37126K->12358K(61440K), 0.0008543 secs]
2.776: [GC (Allocation Failure) [PSYoungGen: 28448K->4096K(28672K)] 36934K->16974K(61440K), 0.0033022 secs]
```

2. Decrease the batch size, which would also give a similar result (ex: optimize data structures to consume less memory). Getting the right solution depends on what is really happening in the application. In some cases, business logic does not permit decreasing batch size. In this case, increasing available memory or redistributing in favor of the young generation might be possible.

## Weak, Soft & Phantom References

Another class of of issues affecting GC is the usage of non-Strong references in applications: Weak references, Soft references, Phantom references. Heavy usage of such references may significantly impact the way garbage collection affects the performance of an application running sometimes to `OutOfMemoryError`.

As a general recommendation, consider enabling the `-XX:+PrintReferenceGC` JVM option to see the impact that different references have on garbage collection. 

### Strong references

This is the default type/class of Reference Object. Any object which has an active strong reference are not eligible for garbage collection. The object is garbage collected only when the variable which was strongly referenced points to null.

```java
MyClass obj = new MyClass (); 
```

Here `obj` object is strong reference to newly created instance of `MyClass`, currently obj is active object so can’t be garbage collected.

```java
obj = null;
//'obj' object is no longer referencing to the instance. 
```

So the `MyClass` type object is now available for garbage collection.

### Weak references

Weak Reference Objects are not the default type/class of Reference Object and they should be explicitly specified while using them.

- This type of reference is used in `WeakHashMap` to reference the entry objects .
- If JVM detects an object with only weak references (i.e. no strong or soft references linked to any object object), this object will be marked for garbage collection.
- To create such references `java.lang.ref.WeakReference` class is used.
- These references are used in real time applications while establishing a DB Connection which might be cleaned up by Garbage Collector when the application using the database gets closed.

```java
//Java Code to illustrate Weak reference 
import java.lang.ref.WeakReference; 
class Gfg 
{ 
    //code 
    public void x() 
    { 
        System.out.println("GeeksforGeeks"); 
    } 
} 
  
public class Example 
{ 
    public static void main(String[] args) 
    { 
        // Strong Reference 
        Gfg g = new Gfg();      
        g.x(); 
          
        // Creating Weak Reference to Gfg-type object to which 'g'  
        // is also pointing. 
        WeakReference<Gfg> weakref = new WeakReference<Gfg>(g); 
          
        //Now, Gfg-type object to which 'g' was pointing earlier 
        //is available for garbage collection. 
        //But, it will be garbage collected only when JVM needs memory. 
        g = null;  
          
        // You can retrieve back the object which 
        // has been weakly referenced. 
        // It successfully calls the method. 
        g = weakref.get();  
          
        g.x(); 
    } 
} 
```

### Soft references

In Soft reference, even if the object is free for garbage collection then also its not garbage collected, until JVM is in need of memory badly. The objects gets cleared from the memory when JVM runs out of memory. To create such references `java.lang.ref.SoftReference` class is used.

```java
//Code to illustrate Soft reference 
import java.lang.ref.SoftReference; 
class Gfg 
{ 
	//code.. 
	public void x() 
	{ 
		System.out.println("GeeksforGeeks"); 
	} 
} 

public class Example 
{ 
	public static void main(String[] args) 
	{ 
		// Strong Reference 
		Gfg g = new Gfg();	 
		g.x(); 
		
		// Creating Soft Reference to Gfg-type object to which 'g' 
		// is also pointing. 
		SoftReference<Gfg> softref = new SoftReference<Gfg>(g); 
		
		// Now, Gfg-type object to which 'g' was pointing 
		// earlier is available for garbage collection. 
		g = null; 
		
		// You can retrieve back the object which 
		// has been weakly referenced. 
		// It successfully calls the method. 
		g = softref.get(); 
		
		g.x(); 
	} 
} 
```

### Phantom references

The objects which are being referenced by phantom references are eligible for garbage collection. But, before removing them from the memory, JVM puts them in a queue called `ReferenceQueue`. They are put in a reference queue after calling finalize() method on them. To create such references `java.lang.ref.PhantomReference` class is used.

```java
//Code to illustrate Phantom reference 
import java.lang.ref.*; 
class Gfg 
{ 
	//code 
	public void x() 
	{ 
		System.out.println("GeeksforGeeks"); 
	} 
} 

public class Example 
{ 
	public static void main(String[] args) 
	{ 
		//Strong Reference 
		Gfg g = new Gfg();	 
		g.x(); 
		
		//Creating reference queue 
		ReferenceQueue<Gfg> refQueue = new ReferenceQueue<Gfg>(); 

		//Creating Phantom Reference to Gfg-type object to which 'g' 
		//is also pointing. 
		PhantomReference<Gfg> phantomRef = null; 
		
		phantomRef = new PhantomReference<Gfg>(g,refQueue); 
		
		//Now, Gfg-type object to which 'g' was pointing 
		//earlier is available for garbage collection. 
		//But, this object is kept in 'refQueue' before 
		//removing it from the memory. 
		g = null; 
		
		//It always returns null. 
		g = phantomRef.get(); 
		
		//It shows NullPointerException. 
		g.x(); 
	} 
} 
```

Execution here leads to Runtime error:

```bash
Exception in thread "main" java.lang.NullPointerException
    at Example.main(Example.java:31)
```

Weak references are actually a lot more common than you might think.  Many **caching solutions** build the implementations using weak referencing, so even if you are not directly creating any in your code, there is a  strong chance your application is still using weakly referenced objects in large quantities.

### Weak Reference Demo application

```java
import java.lang.ref.WeakReference;
import java.util.Arrays;

public class WeakReferences {

    // This example shows how having weak references pointing to objects
    // May result in more frequent Full GC pauses
    //
    // There are two modes (controlled by weak.refs)
    //
    //  1. A lot of objects are created
    //  2. A lot of objects are created, and weak references are created
    //     for them. These references are held in a buffer until it's full
    //
    // The allocations made in both cases need to be exactly the same,
    // so in (1) weak references will be also created, but all of them
    // will be pointing to the same object


    // 1. Run with: -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are mostly young GCs
    //
    // 2. Run with: -Dweak.refs=true -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are mostly full GCs
    //
    // 3. Run with: -Dweak.refs=true -verbose:gc -Xmx64m -XX:NewSize=32m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are mostly young GCs

    private static final int OBJECT_SIZE           = Integer.getInteger("obj.size", 192);
    private static final int BUFFER_SIZE           = Integer.getInteger("buf.size", 64 * 1024);
    private static final boolean WEAK_REFS_FOR_ALL = Boolean.getBoolean("weak.refs");

    private static Object makeObject() {
        return new byte[OBJECT_SIZE];
    }

    public static volatile Object sink;

    public static void main(String[] args) throws InterruptedException {

        System.out.printf("Buffer size: %d; Object size: %d; Weak refs for all: %s%n", BUFFER_SIZE, OBJECT_SIZE, WEAK_REFS_FOR_ALL);

        final Object substitute = makeObject(); // We want to create it in both scenarios so the footprint matches
        final Object[] refs = new Object[BUFFER_SIZE];

        System.gc(); // Clean up young gen

        for (int index = 0;;) {
            Object object = makeObject();
            sink = object; // Prevent Escape Analysis from optimizing the allocation away

            if (!WEAK_REFS_FOR_ALL) {
                object = substitute;
            }

            refs[index++] = new WeakReference<>(object);

            if (index == BUFFER_SIZE) {
                Arrays.fill(refs, null);
                index = 0;
            }
        }
    }
}
```

If we start application with java flags `-Xmx24m -XX:NewSize=16m -XX:MaxTenuringThreshold=1` the GC logs display:

```bash
2.330: [GC (Allocation Failure)  20933K->8229K(22528K), 0.0033848 secs]
2.335: [GC (Allocation Failure)  20517K->7813K(22528K), 0.0022426 secs]
2.339: [GC (Allocation Failure)  20101K->7429K(22528K), 0.0010920 secs]
2.341: [GC (Allocation Failure)  19717K->9157K(22528K), 0.0056285 secs]
2.348: [GC (Allocation Failure)  21445K->8997K(22528K), 0.0041313 secs]
2.354: [GC (Allocation Failure)  21285K->8581K(22528K), 0.0033737 secs]
2.359: [GC (Allocation Failure)  20869K->8197K(22528K), 0.0023407 secs]
2.362: [GC (Allocation Failure)  20485K->7845K(22528K), 0.0011553 secs]
2.365: [GC (Allocation Failure)  20133K->9501K(22528K), 0.0060705 secs]
2.371: [Full GC (Ergonomics)  9501K->2987K(22528K), 0.0171452 secs]
```

Full collections are quite rare in this case.

However, if the application also starts creating weak references (**-Dweak.refs=true**) to these created objects, the situation may change drastically. In any case, making use of weak references here may lead to this:

```bash
2.059: [Full GC (Ergonomics)  20365K->19611K(22528K), 0.0654090 secs]
2.125: [Full GC (Ergonomics)  20365K->19711K(22528K), 0.0707499 secs]
2.196: [Full GC (Ergonomics)  20365K->19798K(22528K), 0.0717052 secs]
2.268: [Full GC (Ergonomics)  20365K->19873K(22528K), 0.0686290 secs]
2.337: [Full GC (Ergonomics)  20365K->19939K(22528K), 0.0702009 secs]
2.407: [Full GC (Ergonomics)  20365K->19995K(22528K), 0.0694095 secs]
```

As we can see, there are now many full collections, and the duration of the collections is an order of magnitude longer! 

The root cause, of course, lies with the weak references. Before we added them, the objects created by the application were dying just  before being promoted to the old generation. A simple solution would be to increase the size of the young generation by specifying `-Xmx64m -XX:NewSize=32m`:

```bash
2.328: [GC (Allocation Failure)  38940K->13596K(61440K), 0.0012818 secs]
2.332: [GC (Allocation Failure)  38172K->14812K(61440K), 0.0060333 secs]
2.341: [GC (Allocation Failure)  39388K->13948K(61440K), 0.0029427 secs]
2.347: [GC (Allocation Failure)  38524K->15228K(61440K), 0.0101199 secs]
2.361: [GC (Allocation Failure)  39804K->14428K(61440K), 0.0040940 secs]
2.368: [GC (Allocation Failure)  39004K->13532K(61440K), 0.0012451 secs]
```

The objects are now reclaimed during minor garbage collection.

### Soft Reference Demo application

```java
import java.lang.ref.SoftReference;
import java.util.Arrays;

public class SoftReferences {

    // This example shows how having soft references pointing to objects
    // May result in more frequent Full GC pauses
    //
    // There are two modes (controlled by soft.refs)
    //
    //  1. A lot of objects are created
    //  2. A lot of objects are created, and soft references are created
    //     for them. These references are held in a buffer until it's full
    //
    // The allocations made in both cases need to be exactly the same,
    // so in (1) soft references will be also created, but all of them
    // will be pointing to the same object


    // 1. Run with: -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are mostly young GCs
    //
    // 2. Run with: -Dsoft.refs=true -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are lots of full GCs
    //
    // 3. Run with: -Dsoft.refs=true -verbose:gc -Xmx64m -XX:NewSize=32m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are still many full GCs

    private static final int OBJECT_SIZE           = Integer.getInteger("obj.size", 192);
    private static final int BUFFER_SIZE           = Integer.getInteger("buf.size", 64 * 1024);
    private static final boolean SOFT_REFS_FOR_ALL = Boolean.getBoolean("soft.refs");

    private static Object makeObject() {
        return new byte[OBJECT_SIZE];
    }

    public static volatile Object sink;

    public static void main(String[] args) throws InterruptedException {

        System.out.printf("Buffer size: %d; Object size: %d; Soft refs for all: %s%n", BUFFER_SIZE, OBJECT_SIZE, SOFT_REFS_FOR_ALL);

        final Object substitute = makeObject(); // We want to create it in both scenarios so the footprint matches
        final Object[] refs = new Object[BUFFER_SIZE];

        System.gc(); // Clean up young gen

        for (int index = 0;;) {
            Object object = makeObject();
            sink = object; // Prevent Escape Analysis from optimizing the allocation away

            if (!SOFT_REFS_FOR_ALL) {
                object = substitute;
            }

            refs[index++] = new SoftReference<>(object);

            if (index == BUFFER_SIZE) {
                Arrays.fill(refs, null);
                index = 0;
            }
        }
    }
}
```

The softly-reachable objects are not reclaimed until the application  risks getting an `OutOfMemoryError`. Replacing weak references with soft  references in the demo application immediately surfaces many more Full  GC events:

```bash
2.162: [Full GC (Ergonomics)  31561K->12865K(61440K), 0.0181392 secs]
2.184: [GC (Allocation Failure)  37441K->17585K(61440K), 0.0024479 secs]
2.189: [GC (Allocation Failure)  42161K->27033K(61440K), 0.0061485 secs]
2.195: [Full GC (Ergonomics)  27033K->14385K(61440K), 0.0228773 secs]
2.221: [GC (Allocation Failure)  38961K->20633K(61440K), 0.0030729 secs]
2.227: [GC (Allocation Failure)  45209K->31609K(61440K), 0.0069772 secs]
2.234: [Full GC (Ergonomics)  31609K->15905K(61440K), 0.0257689 secs]
```

### Phantom Reference Demo application

```java
import java.lang.ref.PhantomReference;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.Arrays;

public class PhantomReferences {

    // This example shows how having phantom references pointing to objects
    // May result in an OutOfMemoryError
    //
    // There are three modes (controlled by phantom.refs and no.ref.clearing)
    //
    //  1. A lot of objects are created
    //  2. A lot of objects are created, and phantom references are created
    //     for them. These references are held in a buffer until it's full.
    //     The reference queue is traversed and cleared on each iteration
    //  3. A lot of objects are created, and phantom references are created
    //     for them. These references are held in a buffer until it's full.
    //     The reference queue is not cleared.
    //
    // The allocations made in both cases need to be exactly the same,
    // so in (1) phantom references will be also created, but all of them
    // will be pointing to the same object


    // 1. Run with: -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are mostly young GCs
    //
    // 2. Run with: -Dphantom.refs=true -verbose:gc -Xmx24m -XX:NewSize=16m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are lots of full GCs
    //
    // 3. Run with: -Dphantom.refs=true -verbose:gc -Xmx64m -XX:NewSize=32m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe that there are few full GCs
    //
    // 4. Run with: -Dphantom.refs=true -Dno.ref.clearing=true -verbose:gc -Xmx64m -XX:NewSize=32m
    //              -XX:MaxTenuringThreshold=1 -XX:-UseAdaptiveSizePolicy
    //
    //    Observe lots of Full GCs and an OutOfMemoryError

    private static final int OBJECT_SIZE           = Integer.getInteger("obj.size", 192);
    private static final int BUFFER_SIZE           = Integer.getInteger("buf.size", 24 * 1024);
    private static final boolean PHANTOM_REFS_FOR_ALL = Boolean.getBoolean("phantom.refs");
    private static final boolean CLEAR_REFS = !Boolean.getBoolean("no.ref.clearing");

    private static Object makeObject() {
        return new byte[OBJECT_SIZE];
    }

    public static volatile Object sink;
    private static final ReferenceQueue<Object> queue = new ReferenceQueue<>();

    public static void main(String[] args) throws InterruptedException {

        System.out.printf("Buffer size: %d; Object size: %d; Phantom refs for all: %s; Clearing refs: %s%n", BUFFER_SIZE, OBJECT_SIZE, PHANTOM_REFS_FOR_ALL, CLEAR_REFS);

        final Object substitute = makeObject(); // We want to create it in both scenarios so the footprint matches
        final Object[] refs = new Object[BUFFER_SIZE];

        System.gc(); // Clean up young gen

        for (int index = 0;;) {
            Object object = makeObject();
            sink = object; // Prevent Escape Analysis from optimizing the allocation away

            if (!PHANTOM_REFS_FOR_ALL) {
                object = substitute;
            }

            refs[index++] = new PhantomReference<>(object, queue);

            if (index == BUFFER_SIZE) {
                Arrays.fill(refs, null);
                index = 0;
            }

            if(CLEAR_REFS) {
                Reference ref;
                while((ref = queue.poll()) != null) {
                    ref.clear();
                }
            }
        }
    }
}
```

Running the demo with the same sets of parameters as before would give us results that are pretty similar as the results in the case with  weak references. The number of full GC pauses would, in fact, be much smaller because of the difference in the finalization described in the beginning of this section.

However, adding one flag that disables phantom reference clearing (`-Dno.ref.clearing=true`) would quickly give us this:

```bash
4.180: [Full GC (Ergonomics)  57343K->57087K(61440K), 0.0879851 secs]
4.269: [Full GC (Ergonomics)  57089K->57088K(61440K), 0.0973912 secs]
4.366: [Full GC (Ergonomics)  57091K->57089K(61440K), 0.0948099 secs]
Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
```

## Humongous allocations

Whenever your application is using the G1 garbage collection algorithm, a phenomenon called humongous allocations can impact your application performance in regards of GC. To recap, humongous allocations are allocations that are larger than 50% of the region size  in G1.

Having frequent humongous allocations can trigger GC performance issues, considering the way that G1 handles such allocations:

- If the regions contain humongous objects, space between the last  humongous object in the region and the end of the region will be unused. If all the humongous objects are just a bit larger than a factor of the region size, this unused space can cause the heap to become fragmented.
- Collection of the humongous objects is not as optimized by the G1 as with regular objects. It was especially troublesome with early Java 8  releases – [until Java 1.8u40](https://bugs.openjdk.java.net/browse/JDK-8027959) the reclamation of humongous regions was only done during full GC events. More recent releases of the Hotspot JVM free the humongous regions at the end of the marking cycle during the cleanup phase, so the impact of the issue has been reduced significantly for newer JVMs.

To check whether or not your application is allocating objects in humongous regions, the first step would be to turn on GC logs similar to the following:

```bash
java -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintReferenceGC -XX:+UseG1GC -XX:+PrintAdaptiveSizePolicy -Xmx128m MyClass
```

Now, when you check the logs and discover sections like these:

```bash
0.106: [G1Ergonomics (Concurrent Cycles) request concurrent cycle initiation, reason: occupancy higher than threshold, occupancy: 60817408 bytes, allocation request: 1048592 bytes, threshold: 60397965 bytes (45.00 %), source: concurrent humongous allocation]
 0.106: [G1Ergonomics (Concurrent Cycles) request concurrent cycle initiation, reason: requested by GC cause, GC cause: G1 Humongous Allocation]
 0.106: [G1Ergonomics (Concurrent Cycles) initiate concurrent cycle, reason: concurrent cycle initiation requested]
 0.106: [GC pause (G1 Humongous Allocation) (young) (initial-mark) 0.106: [G1Ergonomics (CSet Construction) start choosing CSet, _pending_cards: 0, predicted base time: 10.00 ms, remaining time: 190.00 ms, target pause time: 200.00 ms]
```

You have evidence that the application is indeed allocating humongous objects. The evidence is visible in the cause for a GC pause being identified as G1 Humongous Allocation and in the `allocation request: 1048592 bytes` section, where we can see that the application is trying to allocate an object with the size of 1,048,592 bytes, which is 16 bytes larger than  the 50% of the 2 MB size of the humongous region specified for the JVM.

The first solution for humongous allocation is to change the region size so that (most) of the allocations would not exceed the 50% limit triggering allocations in the humongous regions. The region size is calculated by the JVM during startup based on the size of the heap. You can override the size by specifying `-XX:G1HeapRegionSize=XX` in the startup script. The specified region size must be between 1 and 32 megabytes and has to be a power of two.

This solution can have side effects: increasing the region size reduces the number of regions available so you need to be careful and run extra set of tests to see whether or not you actually improved the throughput or latency of the application.

A more time-consuming but potentially better solution would be to understand whether or not the application can limit the size of allocations. The best tools for the job in this case are profilers. They can give you information about humongous objects by showing their allocation sources with stack traces.

# Tools to analyze GC activity

## Built-in application in Java: 

* `jstat`: part of JDK in JAVA_HOME/bin. See https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jstat.html & https://docs.oracle.com/javase/7/docs/technotes/tools/share/jstat.html. You can run `jstat -options` to get the full list of options.

```bash
+-----------------+---------------------------------------------------------------+
|     Option      |                          Displays...                          |
+-----------------+---------------------------------------------------------------+
|class            | Statistics on the behavior of the class loader                |
|compiler         | Statistics  on  the behavior of the HotSpot Just-In-Time com- |
|                 | piler                                                         |
|gc               | Statistics on the behavior of the garbage collected heap      |
|gccapacity       | Statistics of the capacities of  the  generations  and  their |
|                 | corresponding spaces.                                         |
|gccause          | Summary  of  garbage collection statistics (same as -gcutil), |
|                 | with the cause  of  the  last  and  current  (if  applicable) |
|                 | garbage collection events.                                    |
|gcnew            | Statistics of the behavior of the new generation.             |
|gcnewcapacity    | Statistics of the sizes of the new generations and its corre- |
|                 | sponding spaces.                                              |
|gcold            | Statistics of the behavior of the old and  permanent  genera- |
|                 | tions.                                                        |
|gcoldcapacity    | Statistics of the sizes of the old generation.                |
|gcpermcapacity   | Statistics of the sizes of the permanent generation.          |
|gcutil           | Summary of garbage collection statistics.                     |
|printcompilation | Summary of garbage collection statistics.                     |
+-----------------+---------------------------------------------------------------+
```

This tool is extremely useful for getting a quick overview of JVM health to see whether the garbage collector behaves as expected. You can run  it via `jstat -gc -t PID 1s`, for example, where PID is the  process ID of the JVM you want to monitor. You can acquire PID via  running `jps` to get the list of running Java processes (`jps` is also a JAVA_HOME/bin tool). 

```bash
$ jps
6401 Jps
5846 JConsole
```

As a result, each second `jstat` will print a new line to the standard output similar to the following example:

```bash
$ jstat -gc -t 5846 1s
Timestamp        S0C    S1C    S0U    S1U      EC       EU        OC         OU       MC     MU    CCSC   CCSU   YGC     YGCT    FGC    FGCT    CGC    CGCT     GCT   
         1224.5 1024.0 1024.0  0.0   672.0  11776.0   243.1    45056.0    38913.5   28416.0 27637.8 3584.0 3363.0    195    0.624   3      0.194   -          -    0.818
         1225.5 1024.0 1024.0  0.0   672.0  11776.0   243.1    45056.0    38913.5   28416.0 27637.8 3584.0 3363.0    195    0.624   3      0.194   -          -    0.818
         1226.5 1024.0 1024.0  0.0   672.0  11776.0   243.1    45056.0    38913.5   28416.0 27637.8 3584.0 3363.0    195    0.624   3      0.194   -          -    0.818
         1227.5 1024.0 1024.0 704.0   0.0   11776.0   250.0    45056.0    38929.5   28416.0 27637.8 3584.0 3363.0    196    0.626   3      0.194   -          -    0.820
         1228.5 1024.0 1024.0 704.0   0.0   11776.0   250.0    45056.0    38929.5   28416.0 27637.8 3584.0 3363.0    196    0.626   3      0.194   -          -    0.820
         1229.5 1024.0 1024.0 704.0   0.0   11776.0   250.0    45056.0    38929.5   28416.0 27637.8 3584.0 3363.0    196    0.626   3      0.194   -          -    0.820
         1230.5 1024.0 1024.0 704.0   0.0   11776.0   250.0    45056.0    38929.5   28416.0 27637.8 3584.0 3363.0    196    0.626   3      0.194   -          -    0.820
         1231.5 1024.0 1024.0  0.0   704.0  11776.0   268.2    45056.0    38953.5   28416.0 27638.5 3584.0 3363.0    197    0.629   3      0.194
```

| Column | Description                                                  | Jstat Option                                                 |
| ------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| S0C    | Displays the current size of Survivor0 area in KB            | -gc   -gccapacity   -gcnew   -gcnewcapacity                  |
| S1C    | Displays the current size of Survivor1 area in KB            | -gc   -gccapacity   -gcnew   -gcnewcapacity                  |
| S0U    | Displays the current  usage of Survivor0 area in KB          | -gc   -gcnew                                                 |
| S1U    | Displays the current  usage of Survivor1 area in KB          | -gc   -gcnew                                                 |
| EC     | Displays the current size of Eden area in KB                 | -gc   -gccapacity   -gcnew   -gcnewcapacity                  |
| EU     | Displays the current usage of Eden area in KB                | -gc   -gcnew                                                 |
| OC     | Displays the current size of old area in KB                  | -gc   -gccapacity   -gcold   -gcoldcapacity                  |
| OU     | Displays the current usage of old area in KB                 | -gc   -gcold                                                 |
| PC     | Displays the current size of permanent area in KB            | -gc   -gccapacity   -gcold   -gcoldcapacity   -gcpermcapacity |
| PU     | Displays the current usage of permanent area in KB           | -gc   -gcold                                                 |
| YGC    | The number of GC event occurred in young area                | -gc   -gccapacity   -gcnew   -gcnewcapacity   -gcold   -gcoldcapacity   -gcpermcapacity   -gcutil   -gccause |
| YGCT   | The accumulated time for GC operations for Yong area         | -gc   -gcnew   -gcutil   -gccause                            |
| FGC    | The number of full GC event occurred                         | -gc   -gccapacity   -gcnew   -gcnewcapacity   -gcold   -gcoldcapacity   -gcpermcapacity   -gcutil   -gccause |
| FGCT   | The accumulated time for full GC operations                  | -gc   -gcold   -gcoldcapacity   -gcpermcapacity   -gcutil   -gccause |
| GCT    | The total accumulated time for GC operations                 | -gc   -gcold   -gcoldcapacity   -gcpermcapacity   -gcutil   -gccause |
| NGCMN  | The minimum size of new area in KB                           | -gccapacity   -gcnewcapacity                                 |
| NGCMX  | The maximum size of max area in KB                           | -gccapacity   -gcnewcapacity                                 |
| NGC    | The current size of new area in KB                           | -gccapacity   -gcnewcapacity                                 |
| OGCMN  | The minimum size of old area in KB                           | -gccapacity   -gcoldcapacity                                 |
| OGCMX  | The maximum size of old area in KB                           | -gccapacity   -gcoldcapacity                                 |
| OGC    | The current size of old area in KB                           | -gccapacity   -gcoldcapacity                                 |
| PGCMN  | The minimum size of permanent area in KB                     | -gccapacity   -gcpermcapacity                                |
| PGCMX  | The maximum size of permanent area in KB                     | -gccapacity   -gcpermcapacity                                |
| PGC    | The current size of permanent generation area in KB          | -gccapacity   -gcpermcapacity                                |
| PC     | The current size of permanent area in KB                     | -gccapacity   -gcpermcapacity                                |
| PU     | The current usage of permanent area in KB                    | -gc   -gcold                                                 |
| LGCC   | The cause for the last GC occurrence                         | -gccause                                                     |
| GCC    | The cause for the current GC occurrence                      | -gccause                                                     |
| TT     | Tenuring threshold. If copied this amount of times in young  area (S0 ->S1, S1->S0), they are then moved to old area. | -gcnew                                                       |
| MTT    | Maximum Tenuring threshold. If copied this amount of times  inside young arae, then they are moved to old area. | -gcnew                                                       |
| DSS    | Adequate size of survivor in KB                              | -gcnew                                                       |

* `jconsole`: part of OpenJDK in JAVA_HOME/bin since Java 7 update 40. JConsole use JMX API (like VisualVM) to connect with Java application locally or remotely. JConsole is part of Netbeans IDE and access to local or distant application. All the JMX clients run as a separate application connecting to the target JVM. The target JVM can be either local to the client if running both in the same machine, or remote. For the remote connections from client, JVM has to explicitly allow remote JMX connections. This can be achieved by setting a specific system property to the port where you  wish to enable the JMX RMI connection to arrive (the JVM opens port 5432 for JMX connections):

```bash
java -Dcom.sun.management.jmxremote.port=5432 ...
```



![JConsole](/home/vissol/Documents/Personal Articles/images/jconsole.png)



## Standalone applications

- GCViewer: Get the tool here https://github.com/chewiebug/GCViewer and run it simply as a standard Java Swing application.

![GCViewer](/home/vissol/Documents/Personal Articles/images/GCViewer.png)

- VisualVM (http://visualvm.github.io/) with Visual GC plugin (http://visualvm.github.io/plugins.html): VisualVM is also a profiler tool like `hprof` and `AProf`

![](/home/vissol/Documents/Personal Articles/images/VisualVM.png)

- IBM GCMV: https://publib.boulder.ibm.com/httpserv/cookbook/Major_Tools-Garbage_Collection_and_Memory_Visualizer_GCMV.html Eclipse plugin for GC Memory Visualization inside Eclipse IDE. 

![GCVM](/home/vissol/Documents/Personal Articles/images/GCVM.png)



## Online tools

- GCEasy: https://gceasy.io/, exclusively online but very interesting especially if you have a huge set of logs files.
- GCPlot: https://it.gcplot.com/ this tool can be forked on Github in Docker image for an On-Premise installation.

# Memory errors synthesis

| Error                                                        | Cause                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| **java.lang.StackOverFlowError**                             | This error indicates that Stack memory is full.              |
| **java.lang.OutOfMemoryError**                               | This error indicates that Heap memory is full.               |
| **java.lang.OutOfMemoryError: GC Overhead limit exceeded**   | This error indicates that GC has reached its overhead limit  |
| **java.lang.OutOfMemoryError: Permgen space**                | This error indicates that Permanent Generation space is full |
| **java.lang.OutOfMemoryError: Metaspace**                    | This error indicates that Metaspace is full (since Java JDK 8) |
| **java.lang.OutOfMemoryError: Unable to create new native thread** | This error indicates that JVM native code can no longer create a new native  thread from the underlying operating system because so many threads have been already created and they consume all the available memory for the  JVM |
| **java.lang.OutOfMemoryError: request size bytes for reason** | This error indicates that swap memory space is fully consumed by application |
| **java.lang.OutOfMemoryError: Requested array size exceeds VM limit** | This error indicates that our application uses an array size more than the allowed size for the underlying platform |

# Video to watch...

* https://www.youtube.com/watch?v=k4vkd0ahWjQ

# Web sources

- https://www.freecodecamp.org/news/garbage-collection-in-java-what-is-gc-and-how-it-works-in-the-jvm/
- https://plumbr.io/handbook/what-is-garbage-collection
- https://subscription.packtpub.com/book/networking_and_servers/9781849514026/3/ch03lvl1sec23/which-is-the-correct-ratio-between-the-young-and-old-generations
- https://blog.tier1app.com/2016/04/06/gc-logging-user-sys-real-which-time-to-use/
- https://tier1app.files.wordpress.com/2014/12/outofmemoryerror2.pdf
- https://mechanical-sympathy.blogspot.com/2013/07/java-garbage-collection-distilled.html
- https://nipafx.dev/java-unified-logging-xlog/
- https://dzone.com/articles/java-gc-causes-distilled
- https://hub.packtpub.com/getting-started-with-z-garbage-collectorzgc-in-java-11-tutorial/
- https://www.opsian.com/blog/javas-new-zgc-is-very-exciting/
- https://docs.oracle.com/en/java/javase/14/gctuning/garbage-collector-implementation.html#GUID-A24775AB-16A3-4B86-9963-76E5AC398A3E
- https://www.oracle.com/technical-resources/articles/java/g1gc.html
- https://www.oracle.com/technical-resources/articles/java/g1gc.html
- https://blogs.oracle.com/poonam/understanding-g1-gc-logs
- https://dzone.com/articles/heap-memory-in-java-performance-testing
- https://sematext.com/blog/java-garbage-collection-logs/
- https://www.baeldung.com/java-gc-logging-to-file
- https://docs.oracle.com/cd/E19900-01/819-4742/abeik/index.html
- https://docs.oracle.com/javase/8/docs/technotes/guides/vm/gctuning/ergonomics.html
- https://www.geeksforgeeks.org/types-references-java/
- https://dzone.com/articles/understanding-garbage-collection-log
- https://dzone.com/articles/how-monitor-java-garbage
- https://blog.tier1app.com/2016/04/06/gc-logging-user-sys-real-which-time-to-use/
- https://www.freecodecamp.org/news/garbage-collection-in-java-what-is-gc-and-how-it-works-in-the-jvm
- https://dinfuehr.github.io/blog/a-first-look-into-zgc/
- https://docs.oracle.com/en/java/javase/15/gctuning/index.html